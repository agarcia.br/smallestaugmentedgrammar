O pai Goriot
A sra. Vauquer, em solteira De Conflans, � uma senhora de idade que h�
quarenta anos mant�m em Paris uma pens�o burguesa instalada na Rue NeuveSainte-Genevi�ve, entre o Quartier Latin e o Faubourg Saint-Marceau. Essa
pens�o, conhecida pelo nome de Casa Vauquer, admite igualmente homens e
mulheres, mo�os e velhos, sem que nunca a maledic�ncia tenha atacado os
costumes desse respeit�vel estabelecimento. Mas, tamb�m, h� trinta anos nunca
se viu um jovem por l�, e para que um rapaz more ali sua fam�lia deve lhe dar
uma mesada muito magra. No entanto, em 1819, �poca em que come�a este
drama, ali vivia uma pobre mo�a. Seja qual for o descr�dito em que tenha ca�do
a palavra "drama" pela maneira abusiva e torturante como foi atacada nestes
tempos de dolorosa literatura, � necess�rio empreg�-la aqui: n�o que esta hist�ria
seja dram�tica no verdadeiro sentido da palavra; mas, conclu�da a obra, talvez se
ter�o derramado algumas l�grimas intra e extramuros . Ser� ela compreendida
fora de Paris? A d�vida � leg�tima. As peculiaridades desta cena cheia de
observa��es e cores locais s� podem ser aprecidadas entre as colinas de
Montmartre e as alturas de Montrouge, neste ilustre vale de escombros
incessantemente prestes a desabar e de riachos negros de lama; vale repleto de
sofrimentos reais, de alegrias volta e meia falsas, e t�o terrivelmente agitado que
se precisa um n�o sei que de exorbitante para produzir uma sensa��o de certa
perman�ncia. Por�m, a� se encontram, aqui e acol�, dores que pelo amontoado
dos v�cios e virtudes tornam-se grandes e solenes: diante de seu aspecto, os
ego�smos e interesses se det�m e se apiedam; mas a impress�o que recolhem �
como um fruto saboroso prontamente devorado. O carro da civiliza��o,
semelhante ao do �dolo de Jaggernaut, 1 apenas retardado por um cora��o mais
dif�cil de esmagar que os outros e que atravanca a sua roda, logo o quebrou e
prossegue sua marcha gloriosa. Assim far�o voc�s, voc�s que seguram com a
m�o branca este livro, voc�s que se afundam numa poltrona macia pensando:
"Talvez isto v� me divertir". Depois de terem lido os secretos infort�nios do pai
Goriot, jantar�o com apetite imputando a pr�pria insensibilidade ao autor,
tachando-o de exagero, acusando-o de poesia. Ah! saibam: este drama n�o � uma
fic��o, nem um romance. All is true , ele � t�o verdadeiro que todos podem
reconhecer esses elementos em si mesmos, em seu cora��o talvez!

A casa onde se explora a pens�o burguesa pertence � sra. Vauquer. Situa-se na
parte baixa da Rue Neuve-Sainte-Genevi�ve, no lugar onde o terreno desce em
dire��o � Rue de l'Arbal�te por uma ladeira t�o �ngreme e t�o dif�cil que
raramente os cavalos a sobem ou descem. Essa circunst�ncia � favor�vel ao
sil�ncio que reina nessas ruas apertadas entre a c�pula do Val-de-Gr�ce e a
c�pula do Panth�on, dois monumentos que mudam as condi��es da atmosfera,
nela lan�ando tons amarelados e tudo escurecendo com as tonalidades severas
que suas c�pulas projetam. Ali os cal�amentos s�o secos, os riachos n�o t�m
lama nem �gua, o mato cresce ao longo dos muros. Ali o homem mais
indiferente se entristece, como todos os passantes, o barulho de um carro tornase um acontecimento, as casas s�o sombrias, os muros cheiram a pris�o. Ali um
parisiense perdido s� enxergaria pens�es burguesas ou institui��es, 2 mis�ria ou
t�dio, velhice que morre, alegre juventude obrigada a trabalhar. Nenhum bairro
de Paris � mais horr�vel, nem, digamo-lo, mais desconhecido. A Rue NeuveSainte-Genevi�ve �, sobretudo, como uma moldura de bronze, a �nica que
conv�m a este relato, para o qual n�o se deveria preparar demais o esp�rito com
cores escuras, com ideias graves; assim como, de degrau em degrau, o dia
declina e o canto do condutor se acentua quando o viajante desce �s Catacumbas.
3 Compara��o verdadeira! Quem decidir� o que � mais horr�vel ver, cora��es
ressecados ou cr�nios vazios?
A fachada da pens�o d� para um jardinzinho, de modo que a casa cai em
�ngulo reto na Rue Neuve-Sainte-Genevi�ve, onde a vemos cortada em sua
profundidade. Ao longo dessa fachada, entre a casa e o jardinzinho, reina um
c�rculo de cascalhos com dois metros de largura, diante do qual h� uma alameda
arenosa margeada de ger�nios, louros-rosa e rom�zeiras plantados em grandes
vasos de porcelana azul e branca. Entra-se nessa alameda por uma porta
secund�ria, tendo ao alto uma tabuleta em que est� escrito: CASA VAUQUER , e
embaixo: Pens�o burguesa para os dois sexos e outros . 4 Durante o dia, uma
porta com postigo, armada com uma sineta estridente, deixa perceber no final da
cal�adinha, no muro oposto � rua, uma arcada pintada em m�rmore verde por um
artista do bairro. No v�o simulado por essa pintura, eleva-se uma est�tua que
representa o Amor. Ao verem o verniz descascado que a cobre, os amantes de
s�mbolos descobririam talvez um mito do amor parisiense que � curado a poucos
passos dali. 5 Sob o pedestal, esta inscri��o meio apagada lembra o tempo de que
data o ornamento, gra�as ao entusiasmo demonstrado por Voltaire, que retornou
a Paris em 1777:

Quem quer que sejas, eis teu mestre:
Ele o �, o foi ou deve s�-lo. 6
Ao cair a noite, a porta de postigo � substitu�da por uma porta maci�a. O
jardinzinho, t�o largo quanto o comprimento da fachada, fica encravado entre o
muro da rua e a parede-meia da casa vizinha, ao longo da qual pende um manto
de hera que a esconde inteiramente e atrai os olhos dos passantes por um efeito
pitoresco em Paris. Cada um desses muros � forrado de latadas e vinhas cujas
frutifica��es fr�geis e poeirentas s�o alvo dos temores anuais da sra. Vauquer e
de suas conversas com os pensionistas. Ao longo de cada muro reina uma
estreita aleia que leva a uma sombra de t�lias, palavra que a sra. Vauquer,
embora nascida De Conflans, pronuncia obstinadamente t�lhias , apesar das
observa��es gramaticais de seus h�spedes. Entre as duas aleias laterais h� um
canteiro de alcachofras ladeado de �rvores frut�feras podadas em forma de roca,
e margeado de azedinha, alface e salsinha. Sob o abrigo de t�lias est� fincada
uma mesa redonda pintada de verde e cercada de cadeiras. Ali, nos dias
caniculares, os convivas bastante ricos para se permitirem tomar caf� v�o
sabore�-lo num calor capaz de fazer os ovos serem chocados. A fachada, com a
altura de tr�s andares e dominada pelas mansardas, � de pedras e pintada dessa
cor amarela que d� um aspecto ign�bil a quase todas as casas de Paris. As cinco
janelas abertas em cada andar t�m pequenas vidra�as e s�o guarnecidas de
gelosias, nenhuma delas estando levantada da mesma maneira, de modo que
todas as suas linhas discordam entre si. A profundidade dessa casa comporta
duas janelas que, no t�rreo, t�m como ornamento barras de ferro gradeadas.
Atr�s da constru��o h� um quintal com cerca de seis metros de largura, onde
vivem em paz porcos, galinhas, coelhos, e no fundo do qual se ergue um telheiro
para se serrar madeira. Entre esse telheiro e a janela da cozinha est� suspenso o
guarda-comida, abaixo do qual caem as �guas gordurosas da pia. Esse quintal
tem, dando para a Rue Neuve-Sainte-Genevi�ve, uma porta estreita por onde a
cozinheira joga o lixo da casa, limpando essa cloaca com muita �gua, sob pena
de pestil�ncia.
Naturalmente destinado � explora��o da pens�o burguesa, o t�rreo se comp�e
de um primeiro aposento iluminado pelas duas janelas da rua, e no qual se entra
por uma porta-janela. Esse sal�o se comunica com uma sala de jantar que �
separada da cozinha pelo v�o de uma escada cujos degraus s�o de madeira e
ladrilhos pintados e esfregados. Nada � mais triste de ver do que esse sal�o
mobiliado com poltronas e cadeiras estofadas de crina com listas alternadas
foscas e brilhantes. No meio h� uma mesa redonda com tampo de m�rmore
Sainte-Anne, enfeitada com uma licoreira de porcelana branca ornamentada de
filetes de ouro semiapagados, que hoje se encontra por toda parte. Essa sala,
bastante mal assoalhada, � coberta de lambris at� a altura do parapeito. O resto
das paredes � forrado com um papel envernizado representando as principais
cenas de Tel�maco , e cujos person