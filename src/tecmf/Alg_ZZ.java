package tecmf;

import java.io.*;
import java.util.Set;
import java.util.TreeSet;

import tecmf.smst.OGST;
import tecmf.ircoo.IRCOO_Minimizer;
import tecmf.smst.Grammar;
import tecmf.smst.OGrammar;
import tecmf.smst.ONodeInfo;
import tecmf.smst.OOSC;
import tecmf.smst.OSeq;
import tecmf.smst.OSeqLengthComparator;
import tecmf.smst.StringLengthComparator;
import tecmf.utils.P_OSeqSet;
import tecmf.utils.Persistent;

public class Alg_ZZ extends Alg {
    /* Iterative Repeat Replacement */
  

    public String oscType = null;


    P_OSeqSet blacklist = null;
    TreeSet<OSeq> usedpatterns = null;
    TreeSet<OSeq> nonusedpatterns = null;
    OSeq smain = null;
    int initialsize;
    
    public Alg_ZZ(String oscType) {
        super();
        this.oscType = oscType;
        this.debug=false;
    }

    OGrammar minimize (OGrammar g) {
        IRCOO_Minimizer ir = new IRCOO_Minimizer(g, aReverse);
        ir.ircooMinimizeRhs();
        OGrammar g3 = ir.renumber();
        return g3;
    }
    
    int increaseUsedPatternsDecreasesG(TreeSet<OSeq> usedpatterns, TreeSet<OSeq> nonusedpatterns, int size) {
        OGrammar base = new OGrammar(aReverse); 
        base.addRule(smain);
        OSeq candidate = null;
        for (OSeq s:usedpatterns) {
            base.addRule(s);
        }
        for (OSeq s:nonusedpatterns) {
            OGrammar g2 = base.clone();
            g2.addRule(s);
            int newsize = minimize(g2).size();
            if (newsize<size) {
                candidate = s;
                size = newsize;
            }
        }
        if (null !=candidate) {
            assert(initialsize==usedpatterns.size()+nonusedpatterns.size());
            nonusedpatterns.remove(candidate);
            usedpatterns.add(candidate);
            assert(initialsize==usedpatterns.size()+nonusedpatterns.size());
        }
        return size;
    }

    int decreaseUsedPatternsG(TreeSet<OSeq> usedpatterns, TreeSet<OSeq> nonusedpatterns, int size) {
        /* Pode aumentar localmente */
        int localScore = Integer.MAX_VALUE;
        OGrammar base = new OGrammar(aReverse); 
        base.addRule(smain);
        OSeq candidate = null;
        OSeq[] used = (OSeq[]) usedpatterns.toArray(new OSeq[usedpatterns.size()]);
        for (int i = 0; i<used.length; ++i) {
            base.addRule(used[i]);
        }
        for (int i=0; i<base.ruleQtt; ++i) {
            OGrammar g2 = base.clone();
            byte [] nullba = new byte[] {0};
            // ZZ Algorithm will work for ascii files
            // TODO: for binary files singleton rules must be eliminated
            // inside minimize
            g2.setRule((short)i, new OSeq(nullba));
            int newsize = minimize(g2).size();
            if (newsize<localScore) {
                candidate = used[i];
                localScore = newsize;
            }
        }
        if (null !=candidate) {
            System.out.println("Optimizing down");
            assert(initialsize==usedpatterns.size()+nonusedpatterns.size());
            usedpatterns.remove(candidate);
            nonusedpatterns.add(candidate);
            assert(initialsize==usedpatterns.size()+nonusedpatterns.size());
        }
        return localScore;
    }

    public Grammar process(byte buffer[], boolean verbose) {
        blacklist = new P_OSeqSet();
        Alg a = new Alg_CountRepetitions(oscType, blacklist);
        a.process(buffer, verbose);
        initialsize = blacklist.obj.size();
        nonusedpatterns = blacklist.obj;
        usedpatterns = new TreeSet<OSeq>(new OSeqLengthComparator());
        smain =  new OSeq(buffer);
        boolean previousfailure = false;
        int size = Integer.MAX_VALUE;
        TreeSet<OSeq> usedpatternsBeforeDecrease;
        while (true) {
            boolean failure = true;
            while (true) {
                int newsize = increaseUsedPatternsDecreasesG(usedpatterns,nonusedpatterns,size);
                if (newsize<size) {
                    failure = false;
                    size = newsize;
                } else {
                    break;
                }
            }
System.out.println("--usedpatterns.size()="+usedpatterns.size());            
System.out.println("--nonusedpatterns.size()="+nonusedpatterns.size());
System.out.println("size="+size);
OSeq[] used = (OSeq[]) usedpatterns.toArray(new OSeq[usedpatterns.size()]);
for (int i=0; i<used.length;++i) {
    System.out.println("used["+i+"]="+used[i]);
    
}
            usedpatternsBeforeDecrease = (TreeSet<OSeq>)usedpatterns.clone(); 
            int newsize = decreaseUsedPatternsG(usedpatterns,nonusedpatterns,size);
            size = newsize;
            if (newsize<size) {
                // essa situa��o pode acontecer
                failure = false;
            }
System.out.println("usedpatterns.size()="+usedpatterns.size());            
System.out.println("nonusedpatterns.size()="+nonusedpatterns.size());            
System.out.println("size="+size);            
            if (failure && previousfailure) break;
            previousfailure = failure;
            if (failure) {
System.out.println("falha");            
            }
        }
        OGrammar ret = new OGrammar(aReverse); 
        ret.addRule(smain);
        for (OSeq s:usedpatternsBeforeDecrease) {
            ret.addRule(s);
        }        
        return minimize(ret);
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
    }

}
