package tecmf;

import java.io.*;
import java.nio.file.*;

import tecmf.gene.BinaryComplement;
import tecmf.gene.Complement;
import tecmf.gene.DNAComplement;
import tecmf.gene.RNAComplement;
import tecmf.ircoo.IRCOO_Minimizer;
import tecmf.smst.*;
import tecmf.utils.Persistent;

public class MAGR {
    /* Iterative Repeat Replacement */

    static public byte buffer[] = null;
    static public boolean verbose = false;
    static public boolean savebinary = false;
    static public boolean veryVerbose = false;
    static public boolean debug = false;
    static public boolean checkGrammar = false;
    static public boolean printgrammar = false;
    static public boolean printargs = false;
    static public boolean aReverse = false;
    static public Complement  complement = null;
    static public boolean pruneUnused = false;
    static public long saveInterval = -1;
    static public String loadPrefix = null;
    static String oscType = null;
    public static String inputFile = null;
    static int saveStateInterval = -1;
    static public String parameters = "-";
    static public String algName = null;
    static public String sargs = null;

    static public void buffer(String inputFile) throws IOException, FileNotFoundException, SecurityException {
        Path path = FileSystems.getDefault().getPath(".", inputFile);
        int len = (int) Files.size(path);
        buffer = new byte[len];
        InputStream inStream = new BufferedInputStream(new FileInputStream(inputFile));
        int nread = 0;
        while (true) {
            int n = inStream.read();
            if (-1 == n)
                break;
            buffer[nread++] = (byte) n;
            // r0.rhs.add(new Symbol(n));
        }
        inStream.close();
    }

    public static void usage() {
        System.out.println("\nUsage:");
        System.out.println(
                "java -jar MGR.jar [-acfghikloprtBRDv#] <inputFile> [-S saveInterval (in minutes)] [-L timestampLoadPrefix (yyyyMMddHHmmss)] ");
        System.out.println("where:");
        System.out.println("\t a -- print command line Args");
        System.out.println(
                "\t clo -- Occurrence Selector Criterion for IRR/IRCOO/Count: Maximize (c)ompression, (l)ength or (o)ccurrences");
        System.out.println("\t b -- save Binary grammar representation");
        System.out.println("\t g -- Global Optimization of Occurences Replacement - IRCOO algorithm");
        System.out.println("\t h -- help");
        System.out.println("\t i -- IRR algorithm");
        System.out.println("\t k -- checK grammar");
        System.out.println("\t p -- Print generated grammar");
        System.out.println("\t r -- augmented grammar with Reverse");
        System.out.println("\t B -- Binary coMplement");
        System.out.println("\t R -- RNA    coMplement");
        System.out.println("\t D -- DNA    coMplement");
        System.out.println("\t s -- Sequitur algorithm");
        System.out.println("\t t -- process only built in Tests");
        System.out.println("\t u -- prune Unused (or used just once) rules each IRCOO cycle");
        System.out.println("\t v -- verbose");
        System.out.println("\t z -- ZZ algorithm");
        System.out.println("\t # -- count number of repetitions");
    }

    public static String alg() {
        return algName + (null== oscType ?"":"_"+ (oscType));
    }

    public static boolean oscType(String c) {
        if (null != oscType) {
            System.out.println("Occurrence Selector Criterion must be only one of: [clo]");
            usage();
            return false;
        }
        switch (c) {
        case "c":
            oscType = "MC";
            return true;
        case "l":
            oscType = "ML";
            return true;
        case "o":
            oscType = "MO";
            return true;
        }
        return false;
    }

    public static void algName(String s) {
        if (algName ==null) {
            algName = s;
        } else {
            algName = "Error";
        }
        
    }
    public static boolean processArgs(String[] args) {
        /* initialize again so that processArgs() can be repeated in tests */
        verbose = false;
        veryVerbose = false;
        savebinary = false;
        debug = false;
        checkGrammar = false;
        printgrammar = false;
        inputFile = null;
        oscType = null;
        printargs = false;
        aReverse = false;
        complement = null;
        pruneUnused = false;
        saveInterval = -1;
        loadPrefix = null;
        parameters = "-";
        algName = null;

        for (int i = 0; i < args.length; ++i) {
            String s = args[i];
            if (s.startsWith("-")) {
                parameters += s.substring(1);
                if (s.contains("p")) {
                    printgrammar = true;
                }
                if (s.contains("a")) {
                    printargs = true;
                }
                if (s.contains("b")) {
                    savebinary = true;
                }
                if (s.contains("r")) {
                    aReverse = true;
                }
                if (s.contains("B")) {
                    if ( complement == null) {
                        complement=new BinaryComplement();
                    } else {
                        System.out.println("Only one of B, R, D type of complement can be set");
                    }
                }
                if (s.contains("R")) {
                    if ( complement == null) {
                        complement=new RNAComplement();
                    } else {
                        System.out.println("Only one of B, R, D type of complement can be set");
                    }
                }
                if (s.contains("D")) {
                    if ( complement == null) {
                        complement=new DNAComplement();
                    } else {
                        System.out.println("Only one of B, R, D type of complement can be set");
                    }
                }
                if (s.contains("h")) {
                    usage();
                    return false;
                }
                if (s.contains("t")) {
                    algName("Tests");
                }
                if (s.contains("g")) {
                    algName("Ircoo");
                }
                if (s.contains("#")) {
                    algName("Count");
                }
                if (s.contains("i")) {
                    algName("Irr");
                }
                if (s.contains("s")) {
                    algName("Sequitur");
                }
                if (s.contains("z")) {
                    algName("ZZ");
                }
                if (s.contains("u")) {
                    pruneUnused = true;
                }
                if (s.contains("v")) {
                    verbose = true;
                }
                if (s.contains("V")) {
                    verbose = true;
                    veryVerbose = true;
                }
                if (s.contains("k")) {
                    checkGrammar = true;
                }
                if (s.contains("l")) {
                    if (!oscType("l"))
                        return false;
                }
                if (s.contains("c")) {
                    if (!oscType("c"))
                        return false;
                }
                if (s.contains("o")) {
                    if (!oscType("o"))
                        return false;
                }
                if (s.contains("L")) {
                    loadPrefix = args[++i];
                    for (int j = 0; j < loadPrefix.length(); ++j) {
                        if (!Character.isDigit(loadPrefix.charAt(j))) {
                            System.err.println("Wrong timestamp load prefix format:" + loadPrefix);
                            usage();
                            return false;
                        }
                    }
                }
                if (s.contains("S")) {
                    try {
                        saveInterval = Long.parseLong(args[++i]) * 1000 * 60;
                    } catch (Exception e) {
                        System.err.println("Wrong saveInterval in minutes format:" + args[i]);
                        usage();
                        return false;
                    }
                }
            } else if ("".equals(s)) {
                //ok
            } else if (null == inputFile) {
                inputFile = s;
            } else {
                usage();
                return false;
            }
        }
        if (null==algName || "Error".equals(algName)) {
            System.out.println("Exactaly one algorithm can be set, one of: [gis#]");
            usage();
            return false;
        }
        if ("Sequitur".equals(algName)&&null!=oscType) {
            System.out.println("Sequitur algorithm does not support Occurrence Selector Criteria");
            usage();
            return false;
        }
        if (pruneUnused && !"Ircoo".equals(algName)) {
            System.out.println("-u \"Prune Unused rules\" option is only valid for ircoo");
            usage();
            return false;
        }

        if (null == oscType && !"Tests".equals(algName) &&!"Tests".equals(algName)&& !"Sequitur".equals(algName)) {
            System.out.println("Occurrence Selector Criterion must be set, one of: [clo]");
            usage();
            return false;
        }

        if (null == inputFile && !"Tests".equals(algName)) {
            System.out.println("Input file was not set");
            usage();
            return false;
        }
        return true;
    }

    
    public static Grammar process() {
        System.out.println("Starting " + alg() + " algorithm");
        long start = System.currentTimeMillis();
        Alg alg = null;
        if ("Irr".equals(algName)) {
            alg = new Alg_IRR(oscType);
        } else if ("Ircoo".equals(algName)) {
            alg = new Alg_IRCOO(oscType, pruneUnused);
        } else if ("Count".equals(algName)) {
            alg = new Alg_CountRepetitions(oscType,null);
        } else if ("ZZ".equals(algName)) {
            alg = new Alg_ZZ(oscType);
        } else if ("Sequitur".equals(algName)) {
            alg = new Alg_Sequitur();
        }
        Grammar g = alg.process(buffer, verbose);
        long finish = System.currentTimeMillis();
        System.out.println("Processing time:" + (finish - start) + " ms");
        return g;
    }

    public static boolean checkGrammar(Grammar g) {
        return checkGrammar(g, buffer);
    }

    public static boolean checkGrammar(Grammar g, byte buffer[]) {
        if (!checkGrammar)
            return true;
        if (verbose) {
            System.out.println("<checkGrammar>");
        }
//System.out.println("g=" + g);
        OSeq s2 = new OSeq(buffer);
//System.err.println("s2=" + s2 + "* (original)");
        AString a = g.generateString();
        OSeq s1 = (OSeq) a; 
        if (!s1.equals(s2)) {
            System.err.println("ERROR: Grammar Check Failure!");
            System.err.println("s1=" + s1 + "* (generated)");
            System.err.println("s2=" + s2 + "* (original)");
            {
                RuntimeException e = new RuntimeException("Grammar Check Failure");
                e.printStackTrace(System.err);
                System.out.println("g=" + g);
                throw e;
            }
            // return false;
        } else {
            if (verbose)
                System.out.println("Grammar Check Success!");
        }
        if (verbose) {
            System.out.println("</checkGrammar>");
        }
        return true;
    }

    private static void test(String sarg, String input) {
        test(sarg, input.getBytes());
    }

    private static void test(String sarg, byte[] input) {
        buffer = input.clone();
        if (!processArgs(sarg.split(" ", -1))) {
            System.err.println("internal error 1, sarg=" + sarg);
        }
        fillArgs(sarg.split(" ", -1));
        Grammar g = process();
        checkGrammar(g, buffer);
        System.out.println("inputFile=" + inputFile + " g.size()=" + g.size() + " g.encodeSize()=" + g.encodeSize());
        if (printgrammar) {
            System.out.println("g=" + g.toString());
            //((OGrammar)g).generateString();
        }
    }

    private static String teste1 = "xaxbxcxAxbxcxaxBxcxaxbxCxaxcxbxDxbxaxcxExcxbxaxFxaxGxbxHxcx";
    private static String teste2 = "xaxbxAxbxaxB";
    private static String teste3 = "In computer science, a suffix tree (also called PAT tree or, in an earlier form, position tree) is a compressed trie containing all the suffixes of the given text as their keys and positions in the text as their values. Suffix trees allow particularly fast implementations of many important string operations. The construction of such a tree for the string S takes time and space linear in the length of S.";
    private static String teste4 = "axbxcDbxaxcEcxbxa";
    // teste4 d� erro no "_ -kaglr"
    private static String teste5 = "axbxbxaxxbxa";
    // teste5 d� erro no "_ -kaglr"
    private static String teste6 = "axxxaxxbxa";
    // teste6 d� erro no "_ -kaglr"
    private static String teste7 = "axxxaxbxa";
    // teste7 d� erro no "_ -kaglr"
    private static String teste8 = "axxaxbxa";
    // teste8 d� erro no "_ -kaglr" !
    private static String teste9 = "axxaxbxa";
    private static String teste10 = "axbxcDbxaxcEcxbxaaxbxcEcxaxbDcxbxa";
    private static byte[] teste11 = { -128, -120, 0, -128, -120, 0, -128, -120, 0, 10, 50, 127 };
    // private static byte[] teste12= {-128, -120, 0,-128, -120, 0,-128, -120, 0,
    // 102, 127 };

    private static void groupTest(String input, String... garg) {
        for (String s : garg) {
            test("_ " + s, input);
        }
    }

    private static void groupTest(byte[] input, String... garg) {
        for (String s : garg) {
            test("_ " + s, input);
        }
    }

    private static void tests() {
        /* IRR */
        test("_ -kas", teste1);
        test("_ -kasr", teste8);

        test("_ -kaicr", teste4);
        test("_ -kaicr", teste1);
        test("_ -kaicr", teste4);
        test("_ -kaicr", teste10);
        test("_ -kasr", teste1);
        test("_ -kasr", teste3);
        test("_ -kasr", teste10);
        groupTest(teste1,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste2,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste3,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste4,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste5,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste6,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste7,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste8,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste9,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste10,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest(teste11,"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        
        groupTest("atcggtagc".getBytes(),"-kaic","-kaicr","-kail","-kailr","-kaio","-kaior");
        groupTest("atcggtagc".getBytes(),"-kaicD","-kaicrD","-kailD","-kailrD","-kaioD","-kaiorD");
        groupTest("bccZcbbRbbcYccb".getBytes(),"-kaio","-kaior");
        groupTest("bccZcbbRbbcYccb".getBytes(),"-kaicB","-kaicrB","-kailB","-kailrB","-kaioB","-kaiorB");
        
        /* IRCOO */
        //test("_ -kagop", teste1);
        //test("_ -kagorp", teste1);
/*
        groupTest(teste1, "-kagc", "-kagcu", "-kagl", "-kaglu", "-kago", "-kagou");
        groupTest(teste1, "-kagcr", "-kagcur", "-kaglr", "-kaglur", "-kagor", "-kagour");
        groupTest(teste4, "-kagc", "-kagcu", "-kagl", "-kaglu", "-kago", "-kagou");
        groupTest(teste4, "-kagcr", "-kagcur", "-kaglr", "-kaglur", "-kagor", "-kagour");
        groupTest(teste5, "-kagc", "-kagcu", "-kagl", "-kaglu", "-kago", "-kagou");
        groupTest(teste5, "-kagcr", "-kagcur", "-kaglr", "-kaglur", "-kagor", "-kagour");
        groupTest(teste6, "-kagc", "-kagcu", "-kagl", "-kaglu", "-kago", "-kagou");
        groupTest(teste6, "-kagcr", "-kagcur", "-kaglr", "-kaglur", "-kagor", "-kagour");
        groupTest(teste7, "-kagc", "-kagcu", "-kagl", "-kaglu", "-kago", "-kagou");
        groupTest(teste7, "-kagcr", "-kagcur", "-kaglr", "-kaglur", "-kagor", "-kagour");
        groupTest(teste8, "-kagc", "-kagcu", "-kagl", "-kaglu", "-kago", "-kagou");
        groupTest(teste8, "-kagcr", "-kagcur", "-kaglr", "-kaglur", "-kagor", "-kagour");
        groupTest(teste9, "-kagc", "-kagcu", "-kagl", "-kaglu", "-kago", "-kagou");
        groupTest(teste9, "-kagcr", "-kagcur", "-kaglr", "-kaglur", "-kagor", "-kagour");
        groupTest(teste10, "-kagc", "-kagcu", "-kagl", "-kaglu", "-kago", "-kagou");
        groupTest(teste10, "-kagcr", "-kagcur", "-kaglr", "-kaglur", "-kagor", "-kagour");
        groupTest(teste11, "-kagc", "-kagcu", "-kagl", "-kaglu", "-kago", "-kagou");
        groupTest(teste11, "-kagcr", "-kagcur", "-kaglr", "-kaglur", "-kagor", "-kagour");
*/
        System.out.println("Success!");
    }

    private static void fillArgs(String[] args) {
        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (String s : args) {
            sb.append(sep);
            sb.append(s);
            sep = "_";
        }
        sargs=sb.toString();
        if (printargs) {    
            System.out.println(sargs);
        }
    }

    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        if (!processArgs(args))
            return;
        fillArgs(args);
        if (algName.equals("Tests")) {
            tests();
            return;
        }
        if (verbose)
            System.out.println("Reading " + inputFile + " file");
        buffer(inputFile);
        Grammar g = process();
        System.out.println("inputFile=" + inputFile );
        System.out.println(" g.size()/g.encodeSize()/g.ruleQtt=" + g.size()+"\t"+g.encodeSize()
                + "\t" + g.ruleQtt);
        checkGrammar(g, buffer);
        if (printgrammar)
            System.out.println("grammar=" + g.toString());
    }

}
