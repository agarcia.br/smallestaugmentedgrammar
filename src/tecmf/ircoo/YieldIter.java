package tecmf.ircoo;

import java.util.Iterator;

import tecmf.smst.*;

public class YieldIter {

    boolean debug = false;
    String trace = "";

    public OGrammar g;
    public int rule = -1;
    public int i = 0;
    public YieldIter subiter = null;
    public OSeq s = null;
    public int dynamicLen = 0;
    public boolean showTerminator = false;
    public boolean reversed;

    public YieldIter(OGrammar g, int rule, boolean showTerminator, boolean reversed) {
        this.g = g;
        this.rule = rule;
        this.reversed = reversed;
        s = ((OSeq) g.rules.get(rule)).clone();
        if (reversed)
            s = s.reverseRhs(false);
        this.showTerminator = showTerminator;
        if (!showTerminator)
            s = s.subseq(0, s.length() - 1);

    }

    public YieldIter(OGrammar g, OSeq s, boolean showTerminator, boolean reversed) {
        this.g = g;
        this.rule = -1;
        this.reversed = reversed;
        if (reversed)
            s = s.reverseRhs(false);
        this.s = s;
        this.showTerminator = showTerminator;
    }

    public boolean hasNext() {
        if (s.length() > i && (showTerminator || !s.seq[i].isUniqueTerminator())) {
            if (!s.seq[i].isNonterminal()) {
                return true;
            } else {
                if (null == subiter) {
                    /* 1a vez na posicao */
                    return true;
                } else {
                    return subiter.hasNext()
                            || (s.seq.length > (i + 1) && (showTerminator || !s.seq[i + 1].isUniqueTerminator()));
                }
            }
        }
        return false;
    }

    public ASymbol next() {
        if (debug)
            System.out.println("<next()> this=" + this + " i=" + i);
        dynamicLen++;
        if (null != subiter) {
            if (subiter.hasNext()) {
                /* nao altera o i */
                ASymbol ret = subiter.next();
                if (debug)
                    System.out.println(trace + " 1 " + ret);
                return ret;
            } else {
                subiter = null;
                ++i;
            }
        }

        if (!showTerminator && s.seq[i].isUniqueTerminator())
            throw new RuntimeException("passed the limit of iterator");
        assert (showTerminator || !s.seq[i].isUniqueTerminator());
        if (!s.seq[i].isNonterminal()) {
            /* altera o i */
            ASymbol ret = s.seq[i++];
            if (debug)
                System.out.println(trace + " 2 " + ret);
            return ret;
        }
        assert (s.seq[i].isNonterminal());
        // ONE LEVEL REVERSE
        // subiter = new YieldIter(g,s.seq[i].getValue(),false, s.seq[i].isReversed());
        // RECURSIVE SEMANTICS
        subiter = new YieldIter(g, s.seq[i].getValue(), false, s.seq[i].isReversed() ^ reversed);

        /* nao altera o i */
        if (debug)
            System.out.println("<next()/> i=" + i + " seq[" + i + "]=" + s.seq[i]);
        ASymbol ret = subiter.next();
        if (debug)
            System.out.println(trace + " 3 " + ret);
        return ret;
    }

    public static void main(String args[]) {
        OGrammar g = new OGrammar(true);
        OSeq os = new OSeq("xaxbxcxAxbxcxaxBxcxaxbxCxaxcxbxDxbxaxcxExcxbxaxFxaxGxbxHxcx");
        g.addRule(os);
        System.out.println("**os=" + os);
        String s1 = os.toString();
        int a = 0;
        while (true) {
            // 1. montar Suffix Tree
            OGST gst = g.getRhsGST(false, false,false);
            // 2. encontrar maior subpadrao
            ONodeInfo ni = (new OOSC_ML(gst, null, g)).selectOcurrence();
            // 3 se nao encontrou, sair
            if (!ni.nonOverlaopingOcurrences)
                break;
            // 4. incluir nova regra na gramatica
            Integer length = ni.node.getPattern().length();
            g.addRule(ni.node.getPattern());
            // 5. substituir padrao nas regras
            Iterator<Ocurrence> it = ni.ocurrences.iterator();
            int oldrule = -1;
            int ruleTotalRhsLenBeforeChange = -1;
            while (it.hasNext()) {
                /*
                 * 1. Must be iterator to assure left to right scan 2. Since a single rule may
                 * change more times, its length must be kept to calculatre adjustedLeftPos
                 */
                Ocurrence oc = it.next();
                int rule = oc.nonTerminal.getValue();
                if (rule != oldrule) {
                    oldrule = rule;
                    ruleTotalRhsLenBeforeChange = g.rule(rule).length();
                }
                ((OSeq) g.rules.get(oc.nonTerminal.getValue())).reduce(oc, length, (short) (g.ruleQtt - 1),
                        ruleTotalRhsLenBeforeChange);
            }
            a++;
            if (a > 3)
                break;
        }
        System.out.println("**g=" + g);
        System.out.println("**g.rules.size()=" + g.rules.size());
        YieldIter y = new YieldIter(g, 0, false, false);
        StringBuilder sb2 = new StringBuilder();
        while (y.hasNext()) {
            sb2.append(y.next().toString());
        }
        String s2 = sb2.toString();
        System.out.println("s2=" + s2);
        System.out.println("r0>s1.equals(s2)=" + s1.equals(s2));

        sb2 = new StringBuilder();
        y = new YieldIter(g, (OSeq) g.rules.get(1), false, false);
        while (y.hasNext()) {
            sb2.append(y.next().toString());
        }
        s2 = sb2.toString();
        System.out.println("s2=" + s2);

        sb2 = new StringBuilder();
        y = new YieldIter(g, (OSeq) g.rules.get(3), false, true);
        while (y.hasNext()) {
            sb2.append(y.next().toString());
        }
        s2 = sb2.toString();
        System.out.println("reversed rule 3=" + s2);

        sb2 = new StringBuilder();
        y = new YieldIter(g, (OSeq) g.rules.get(1), false, true);
        while (y.hasNext()) {
            sb2.append(y.next().toString());
        }
        s2 = sb2.toString();
        System.out.println("reversed rule 1=" + s2);

        sb2 = new StringBuilder();
        y = new YieldIter(g, 0, true, false);
        while (y.hasNext()) {
            sb2.append(y.next().toString());
        }
        s2 = sb2.toString();
        System.out.println("r0>>s2=" + s2);
        System.out.println("s1.equals(s2)=" + s1.equals(s2.substring(0, s2.length() - 2)));

        y = new YieldIter(g, (OSeq) g.rules.get(1), false, true);
        while (y.hasNext()) {
            System.out.print(y.next().toString());
        }
        System.out.println();

    }

}
