package tecmf.ircoo;

import java.util.Comparator;

public class RuleComparator implements Comparator<Rule> {
    @Override
    public int compare(Rule p1, Rule p2) {
        /* reverse order to get higher yieldLen first */
        if (p1.yieldLen < p2.yieldLen) {
            return 1;
        } else if (p1.yieldLen > p2.yieldLen) {
            return -1;
        }
        return 0;
    }
}