package tecmf.ircoo;

import java.io.*;
import java.util.*;

import tecmf.smst.*;

public class IRCOO_Minimizer {

    static public boolean debug = false;

    OGrammar g;
    boolean aReverse;
    short ruleMap[];
    short usecount[];
    short newNumber[];
    PriorityQueue<Rule> ruleQueue;
    public int yieldSize[] = null;

    public IRCOO_Minimizer(OGrammar g, boolean aReverse) {
        this.g = g;
        this.aReverse = aReverse;
        ruleMap = new short[g.ruleQtt];
        usecount = new short[g.ruleQtt];
        newNumber = new short[g.ruleQtt];
        ruleQueue = new PriorityQueue<Rule>(10, new RuleComparator());
        usecount[0] = 2; // rule 0 must be kept
    }

    private short getFromQueue() {
        short r = -1;
        try {
            r = ruleQueue.remove().rule;
        } catch (NoSuchElementException ignore) {
        }
        return r;
    }

    public int getYieldSize(short r) {
        if (null == yieldSize) {
            yieldSize = new int[g.ruleQtt];
        }
        if (0 == yieldSize[r]) {
            OSeq rhs = (OSeq) g.rules.get(r);
            for (ASymbol s : rhs.seq) {
                if (s.isNonterminal()) {
                    yieldSize[r] += getYieldSize(s.getValue());
                } else {
                    yieldSize[r]++;
                }
            }
        }
        return yieldSize[r] - 1; // no end marker
    }

    public void ircooMinimizeRhs() {
        for (short i = 0; i < g.ruleQtt; ++i) {
            Rule r2 = new Rule();
            r2.rule = i;
            r2.yieldLen = getYieldSize(i);
            ruleQueue.add(r2);
        }
        short r = getFromQueue();
        // OGrammar gtemp = g.clone();
        while (-1 != r) {
            if (debug) {
                System.out.println("minimizing rule=" + r);
                System.out.println("dentro IRCOO_Minimizer");
                if (g.ruleQtt > 9)
                    System.out.println("((OSeq)g.rule((short)9)).seq[0]=" + ((OSeq) g.rule((short) 9)).seq[0]);
            }
            YieldDag.debug = debug;
            YieldDag y = new YieldDag(g, r, aReverse);
            OSeq rhs = y.shortestPath();
            if (debug)
                System.out.println("rule=" + r + " old rhs=" + g.rule(r));
            if (debug)
                System.out.println("rule=" + r + " rhs=" + rhs);
            // gtemp.setRule(r, rhs);
            g.setRule(r, rhs);
            // if (debug) System.out.println("**** DUMPG g="+g);
            for (int i = 0; i < rhs.length(); ++i) {
                if (rhs.seq[i].isNonterminal()) {
                    short rule = rhs.seq[i].getValue();
                    usecount[rule]++;
                }
            }
            r = getFromQueue();
        }
        // g = gtemp;
        if (debug) {
            System.out.println("in <ircooMinimizeRhs>:");
            for (int i = 0; i < g.ruleQtt; ++i) {
                System.out.println("usecount[" + i + "]=" + usecount[i]);
            }
        }
    }

    private OSeq renumberRhs(OSeq s) {
        OSeq s2 = s.clone();
        for (int i = 0; i < s2.length(); ++i) {
            ASymbol original = s2.seq[i];
            if (original.isNonterminal()) {
                short rule = original.getValue();
                if (0 == usecount[rule]) {
                    continue;
                }
                if (1 == usecount[rule]) {
                    s = s.expandRule(rule, (OSeq) g.rules.elementAt(rule), true, true);
                    return renumberRhs(s);
                }
                assert (usecount[rule] > 1);
                s2.seq[i] = ASymbol.nonTerminal(newNumber[rule]);
                if (aReverse) {
                    s2.seq[i].setReversed(original.isReversed());
                }
            }
        }
        return s2;
    }

    boolean loopCondition() {
        boolean ret = false;
        short newRule = (short) (g.ruleQtt - 1);
        if (usecount[newRule] < 2) {
            /* new Rule will be eliminated!!, we may loop */
            ret = true;
            for (int i = 1; i < newRule; ++i) {
                if (usecount[i] < 2) {
                    /*
                     * ok, eliminating another rule will prevent loop, (unless it is recreated in
                     * next iteration and newRule eliminated)!
                     */
                    ret = false;
                    break;
                }
            }
        }
        if (ret) {
            /*
             * This condition is no longer needed because of the external grammar size check
             * but it still SAVES a little processing time but it may LEAVE a RULE BEING
             * USED ONCE, NOT GOOD
             */
            if (debug)
                System.err.println("Loop condition prevented new rule elimination");
        }
        return ret;
    }

    public OGrammar renumber() {
        /*
         * for (short r=0; r<g.ruleQtt; ++r) {
         * System.out.println("rule="+r+" usecount[r]="+usecount[r]); }
         */

        boolean needToRenumber = false;
        short j = 0;
        for (short i = 0; i < g.ruleQtt; ++i) {
            if (usecount[i] > 1) {
                newNumber[i] = j++;
            } else {
                /* unnecessary, but will raise exception */
                newNumber[i] = -1;
                needToRenumber = true;
            }
        }
        if (needToRenumber) {
            OGrammar g2 = new OGrammar(true);
            for (short i = 0; i < g.ruleQtt; ++i) {
                if (usecount[i] > 1) {
                    OSeq rn = renumberRhs(((OSeq) g.rules.elementAt(i)).subseq(0, g.rules.elementAt(i).length() - 1));
                    g2.addRule(rn);
                }
            }
            return g2;
        } else {
            return g;
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
    }

}
