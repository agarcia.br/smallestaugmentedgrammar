package tecmf.ircoo;

import java.util.*;

import tecmf.smst.*;

public class YieldDag {

    static public boolean debug = false;
    public OGrammar g;
    // public short rule;
    OSeq pattern;
    public HashMap<ASymbol, Integer> links[];
    public HashSet<YieldIter> ys;
    boolean reverse;
    short ruleToMinimizeRhs;

    public YieldDag(OGrammar g, short ruleToMinimizeRhs, boolean reverse) {
        this.g = g;
        /*
         * iterate is not desirable, but at least is O(n) and if not used would
         * complicate backwards search in shortestPath();
         */
        pattern = iterate(ruleToMinimizeRhs);
        if (debug)
            System.out.println("pattern=" + pattern.toString());
        ys = new HashSet<YieldIter>();
        this.ruleToMinimizeRhs = ruleToMinimizeRhs;
        this.reverse = reverse;
    }

    private OSeq iterate(short pRule) {
        OSeq s = new OSeq();
        YieldIter y = new YieldIter(g, pRule, false, false);
        while (y.hasNext()) {
            s.append(y.next());
        }
        return s;
    }

    public void dump() {
        if (null == links)
            assignLinks();
        System.out.println("main pattern:" + pattern.toString());
        // System.out.println("main rule yield:"+g.yield(rule));
        for (int i = 0; i < pattern.length(); ++i) {
            HashMap<ASymbol, Integer> li = links[i];
            if ((null != li) && !li.isEmpty()) {
                System.out.println("Position:" + i);
                Set<ASymbol> keys = li.keySet();
                for (ASymbol key : keys) {
                    System.out.println("\t" + key + "->" + li.get(key));
                }
            }

        }
    }

    public short first[] = null;
    public short last[] = null;

    protected short getFirst(short r) {
        if (0 != first[r])
            return first[r];
        AString rule = g.rules.get(r);
        ASymbol firstSymbol = rule.getFirst();
        if (firstSymbol.isTerminal()) {
            /* terminal */
            return firstSymbol.getValue();
        } else {
            /* mesmo first que o nao terminal */
            assert (firstSymbol.isNonterminal());
            if (firstSymbol.isReversed()) {
                return getLast(firstSymbol.getValue());
            } else {
                return getFirst(firstSymbol.getValue());
            }
        }
    }

    protected short getLast(short r) {
        if (0 != last[r])
            return last[r];
        AString rule = g.rules.get(r);
        ASymbol lastSymbol = rule.getLastNonTerminator();
        if (lastSymbol.isTerminal()) {
            /* terminal */
            return lastSymbol.getValue();
        } else {
            /* mesmo first que o nao terminal */
            assert (lastSymbol.isNonterminal());
            if (lastSymbol.isReversed()) {
                return getFirst(lastSymbol.getValue());
            } else {
                return getLast(lastSymbol.getValue());
            }
        }
    }

    protected short getFirstOrLast(short r, boolean reversed) {
        return reversed ? getLast(r) : getFirst(r);
    }

    public void startFirstAndLast() {
        first = new short[g.ruleQtt];
        last = new short[g.ruleQtt];
    }

    public void assignLinks() {
        startFirstAndLast();
        links = new HashMap[pattern.length()];
        if (debug)
            System.out.println("<assignLinks>");
        if (debug)
            System.out.println("pattern(nnt)=" + pattern.toString());
        if (debug)
            System.out.println("");
        /* para usar esse loop abaixo, o padrao nao pode ter nao terminais */
        for (int i = 0; i < pattern.length(); ++i) {
            /*
             * Time = O(n)O(r + rn log nr) = O(rn^2 log nr)
             */
            if (debug)
                System.out.println("i=" + i + "pattern[i]=" + pattern.seq[i].toString());
            /* para cada letra do padrao */
            Iterator<YieldIter> it = ys.iterator();
            while (it.hasNext()) {
                /*
                 * Number of members in it is O(rn) Time O(rn log nr)
                 */
                YieldIter y = it.next();
                if (debug)
                    System.out.println("Encontrado iter y, y.r=" + y.rule + ":" + y.s.toString());
                assert (y.hasNext());
                ASymbol next = y.next();
                if (debug)
                    System.out.println("Encontrado iter y, y.next()=" + next.toString());
                if (next.equals(pattern.seq[i])) {
                    if (debug) {
                        System.out.println("next==pattern.seq[i]");
                    }
                    if (!y.hasNext()) {
                        if (debug)
                            System.out.println("!y.hasNext()");
                        assert (!next.isUniqueTerminator()); /* nao inclui terminacao */
                        /* pattern nunca inclui terminador <0, pois este nao repete */
                        // esse length s� vai funcionar se n�o houver vari�vel
                        // poderia calcular 1 sob demanda
                        int ylength = y.dynamicLen;
                        if (debug)
                            System.out.println("y.s.length()=" + y.s.length());
                        if (debug)
                            System.out.println("y.dynamicLen=" + y.dynamicLen);
                        int end = i + 1;
                        int begin = end - ylength;
                        if (debug)
                            System.out.println("end=" + end);
                        if (debug)
                            System.out.println("begin=" + begin);
                        if (null == links[begin]) {
                            links[begin] = new HashMap<ASymbol, Integer>();
                        }
                        /*
                         * #links[begin] = O(nr) Time put() = O(log nr) Space #links[begin] = O(nr)
                         * Space links = O(n^2r)
                         */
                        ASymbol nt = ASymbol.nonTerminal((short) y.rule);
                        nt.setReversed(y.reversed);
                        links[begin].put(nt, end);
                        it.remove();
                    }
                } else {
                    if (debug) {
                        System.out.println("next!=pattern.seq[i]");
                    }
                    it.remove();
                }
            } /* while (it.hasNext()) */
            /*
             * Time O(r) Space O(r) -- current implementation of YieldIter(g,r) is O(r), so
             * current space is O(r^2)
             */
            // for (short r=1; r<g.rules.size(); ++r) {
            for (short r = (short) (ruleToMinimizeRhs + 1); r < g.rules.size(); ++r) {
                // pode come�ar em (ruleToMinimizeRhs+1)
                // porque agora est� em ordem decrescente de tamanho
                // if (forbiddenRules[r]) continue;
                if (g.rule(r).length()==2) {
                    //System.out.println(">>>"+g.rule(r).toString());
                    assert(((OSeq)g.rule(r)).seq[0].isTerminal());
                    assert(((OSeq)g.rule(r)).seq[0].getValue()==0);
                    continue;
                }
                if (getFirst(r) == pattern.seq[i].getValue()) {
                    YieldIter y2 = new YieldIter(g, r, false, false);
                    if (debug) {
                        System.out.println("CRIADO iter y2, y2.rule=" + y2.rule + ":" + y2.s.toString());
                        System.out.println("***pattern.seq[" + i + "]=" + pattern.seq[i]);
                        System.out.println("***getFirst(" + r + ")=" + getFirst(r));
                    }
                    y2.next();
                    ys.add(y2);
                } else {
                    if (debug & !reverse) {
                        System.out.println("NAO CRIADO iter");
                        System.out.println("***pattern.seq[" + i + "]=" + pattern.seq[i]);
                        System.out.println("***getFirst(" + r + ")=" + getFirst(r));
                    }
                }
                if (reverse && getLast(r) == pattern.seq[i].getValue()) {
                    YieldIter y2 = new YieldIter(g, r, false, true);
                    if (debug) {
                        System.out.println("CRIADO iter y2, y2.rule=" + y2.rule + ":" + y2.s.toString());
                        System.out.println("***(R)pattern.seq[" + i + "]=" + pattern.seq[i]);
                        System.out.println("***(R)getLast(" + r + ")=" + getLast(r));

                    }
                    y2.next();
                    ys.add(y2);
                }
            }
        }
    }

    public OSeq shortestPath() {
        /*
         * n = pattern.length() r = g.ruleQtt Time complexity = O(n.r log(nr)) Space
         * complexity = O(n)
         */
        if (null == links)
            assignLinks();
        // if (debug) dump();
        int distance[] = new int[pattern.length() + 1];
        ASymbol symbol[] = new ASymbol[pattern.length()];
        int jump[] = new int[pattern.length()];
        int i;
        for (i = pattern.length() - 1; i >= 0; --i) {
            distance[i] = distance[i + 1] + 1;
            symbol[i] = pattern.seq[i];
            jump[i] = 1;
            HashMap<ASymbol, Integer> hm = links[i];
            if (null == hm) {
                continue;
            }
            for (ASymbol r : hm.keySet()) {
                // links[begin].put((short)(y.rule+Grammar.IRR), end);
                // Time get() = O(log nr)
                int end = hm.get(r);
                assert (end > i);
                int newdistance = 1 + distance[end];
                if (newdistance < distance[i]) {
                    distance[i] = newdistance;
                    symbol[i] = r;
                    jump[i] = end - i;
                    assert (jump[i]) > 0;
                }
            }
        }
        ASymbol sPath[] = new ASymbol[pattern.length()];
        int sPathLen = 0;
        for (i = 0; i < pattern.length(); i += jump[i]) {
            if (debug)
                System.out.println("symbol[" + i + "]=" + symbol[i]);
            sPath[sPathLen++] = symbol[i];
        }
        if (debug)
            System.out.println("sPathLen=" + sPathLen);
        return new OSeq(sPath, sPathLen);
    }

    public static void main(String args[]) {
        OGrammar g = new OGrammar(true);
        OSeq os = new OSeq("xaxbxcxAxbxcxaxBxcxaxbxCxaxcxbxDxbxaxcxExcxbxaxFxaxGxbxHxcx");
        g.addRule(os);
        Set<OSeq> blacklist = new HashSet<OSeq>();
        int a = 0;
        IRCOO_Minimizer ir = null;
        while (true) {
            a++;
            if (debug)
                System.out.println("GRAMMAR:");
            if (debug)
                System.out.println(g.toString());
            // 1. montar Suffix Tree
            OGST gst = g.getYieldGST(false);
            // 2. encontrar maior subpadrao
            ONodeInfo ni = (OOSC.getOSC("ML", gst, blacklist, g)).selectOcurrence();
            // 3 se nao encontrou, sair
            if (!ni.nonOverlaopingOcurrences)
                break;
            // 4. incluir nova regra na gramatica
            OSeq p = ni.node.getPattern();
            System.out.println("ni.node pattern=" + p.toString());
            // Integer length = p.length();
            System.out.println("***********************************");
            System.out.println("ANTES g=" + g);
            OGrammar g2 = g.clone();
            g2.addRule(p);
            ir = new IRCOO_Minimizer(g2, false);
            // 5. minimiza todos os yields[r];
            System.out.println("***********************************");
            System.out.println("ANTES g2=" + g2);
            ir.ircooMinimizeRhs();
            // 6. Elimina as regras usadas 0 ou 1 vez.
            System.out.println("***********************************");
            System.out.println("DEPOIS g2=" + g2);
            System.out.println("***********************************");
            OGrammar g3 = ir.renumber();
            boolean updated = false;
            if (g3.size() < g.size()) {
                updated = true;
                g = g3;

                System.out.println("Grammar updated because g3.size()<g.size()");
            } else {
                System.out.println("Grammar not updated because g3.size()>=g.size()");
            }
            {
                String pa = p.toString();
                int len = pa.length();
                if (len > 10)
                    len = 10;
                System.out.println("iterations=" + a + "; numrules=" + g.ruleQtt + " pattern =" + pa.substring(0, len)
                        + " pattern.length()=" + p.toString().length() + " grammar updated=" + updated);
            }
        }
        System.out.println("g=" + g);
    }

}
