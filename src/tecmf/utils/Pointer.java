package tecmf.utils;

public class Pointer<T> {
    public T obj;

    public Pointer(T obj) {
        this.obj = obj;
    }
}
