package tecmf.utils;

import java.util.Objects;

@SuppressWarnings("rawtypes")
public class Pair<F, S> implements Comparable {
    public final F first;
    public final S second;

    public Pair(F first, S second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return first.toString() + ":" + second.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair)) {
            return false;
        }
        Pair<?, ?> p = (Pair<?, ?>) o;
        return Objects.equals(p.first, first) && Objects.equals(p.second, second);
    }

    @Override
    public int hashCode() {
        return (first == null ? 0 : first.hashCode()) ^ (second == null ? 0 : second.hashCode());
    }

    @Override
    public int compareTo(Object o) {
        /*
         * The order of ocurrences to work with substitution must be: 1. grouped by
         * Short (rule/string number) 2. decreasing order by Integer (position)
         */
        if ((this instanceof Pair<?, ?>) && (o instanceof Pair<?, ?>) && (this.first instanceof Short)
                && (((Pair<?, ?>) o).first instanceof Short) && (this.second instanceof Integer)
                && (((Pair<?, ?>) o).second instanceof Integer)) {
            @SuppressWarnings("unchecked")
            Pair<Short, Integer> o1 = (Pair<Short, Integer>) this;
            @SuppressWarnings("unchecked")
            Pair<Short, Integer> o2 = (Pair<Short, Integer>) o;
            if (o1.first < o2.first) {
                return -1;
            } else if (o1.first > o2.first) {
                return 1;
            } else {
                if (o1.second < o2.second) {
                    return 1;
                } else if (o1.second > o2.second) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
        return 0;
    }

}
