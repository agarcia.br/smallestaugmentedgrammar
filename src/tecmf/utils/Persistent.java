package tecmf.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import tecmf.MAGR;

public class Persistent implements Serializable {

    private static final long serialVersionUID = -5413525899113227574L;

    public long lastSave;

    public Persistent() {
        lastSave = System.currentTimeMillis();
    }

    static int count=0;
    public static void conditionalSave(long interval, Persistent... pa) {
        if (0 > interval)
            return;
        long currentTime = System.currentTimeMillis();
        // suppose all objects are persisted together and have same lastSave
        if (currentTime - pa[0].lastSave < interval) {
            if (MAGR.veryVerbose && 0==(++count)%200) {
                System.out.println("Interval="+interval+ " not reached:"+(currentTime - pa[0].lastSave));
            }
            return;
        }
        Format formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeprefix = formatter.format(new Date(currentTime));
        for (Persistent p : pa) {
            p.save(timeprefix);
        }
    }

    public static String prefix(Class c) {
        return "MAGR_"+MAGR.alg()+"_"+MAGR.inputFile+"_"+c.getSimpleName();
    }
    
    public boolean save(String timestamp) {
        boolean ret = false;
        ObjectOutputStream oos = null;
        try {
            FileOutputStream fout = new FileOutputStream(prefix(this.getClass()) + "_" + timestamp, false);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(this);
            ret = true;
            oos.close();
            lastSave = System.currentTimeMillis();
        } catch (Exception e) {
            try {
                oos.close();
            } catch (Exception ignore) {
            }
            e.printStackTrace(System.err);
        }
        return ret;
    }

    static public Object load(Class c, String timestamp) {
        Object ret = null;
        ObjectInputStream objectinputstream = null;
        try {
            FileInputStream streamIn = new FileInputStream(prefix(c) + "_" + timestamp);
            objectinputstream = new ObjectInputStream(streamIn);
            ret = objectinputstream.readObject();
            objectinputstream.close();
        } catch (Exception e) {
            try {
                objectinputstream.close();
            } catch (Exception ignore) {
            }
            e.printStackTrace();
        }
        return ret;
    }

    static public void saveObjets(Persistent... pa) {
    }

}
