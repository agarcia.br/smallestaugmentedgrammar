package tecmf.utils;

import java.util.AbstractQueue;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import tecmf.smst.OSeq;

public class DFS {

    Set<Short> graph[];
    public DFS(Set<Short> graph[]) {
        this.graph=graph;
    }
    
    static final private byte WHITE=0;
    static final private byte GRAY=1;
    static final private byte BLACK=2;
    
    public Deque<Short> sort;
    byte color[];
    private void dfs_visit(short u) {
        color[u] = GRAY;
        for (short v: graph[u]) {
            if (WHITE==color[v]) {
                dfs_visit(v);
            }
        }
        color[u] = BLACK;
        sort.addLast(u);
    }
      
    public void dfs() {
        color = new byte[graph.length];
        sort = new ArrayDeque<Short>();
        dfs_visit((short)0);
    }
}
