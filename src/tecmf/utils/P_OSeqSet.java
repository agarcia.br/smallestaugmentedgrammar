package tecmf.utils;

import java.util.TreeSet;
import tecmf.smst.OSeq;
import tecmf.smst.OSeqLengthComparator;

public class P_OSeqSet extends Persistent {
    private static final long serialVersionUID = 7199901628772961036L;
    public TreeSet<OSeq> obj;

    public P_OSeqSet clone() {
        P_OSeqSet ret = new P_OSeqSet();
        ret.obj = new TreeSet<OSeq>(new OSeqLengthComparator());
        ret.obj.addAll(obj);
        return ret;
    }
}
