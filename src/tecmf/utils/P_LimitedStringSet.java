package tecmf.utils;

import java.util.Set;

public class P_LimitedStringSet extends Persistent {
    private static final long serialVersionUID = 7199901628772961036L;
    public LimitedQueue<String> obj;

    public P_LimitedStringSet(int limit) {
        obj = new LimitedQueue<String>(limit);
    }

    public boolean contains(String s) {
        if (null == obj)
            return false;
        else
            return obj.contains(s);
    }

    public boolean add(String s) {
        if (null == obj)
            return false;
        else
            return obj.add(s);
    }

}
