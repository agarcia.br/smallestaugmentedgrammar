package tecmf;

import java.io.*;
import java.util.Set;
import java.util.TreeSet;

import tecmf.smst.OGST;
import tecmf.ircoo.IRCOO_Minimizer;
import tecmf.smst.Grammar;
import tecmf.smst.OGrammar;
import tecmf.smst.ONodeInfo;
import tecmf.smst.OOSC;
import tecmf.smst.OSeq;
import tecmf.smst.OSeqLengthComparator;
import tecmf.smst.StringLengthComparator;
import tecmf.utils.P_OSeqSet;
import tecmf.utils.Persistent;

public class Alg_IRCOO extends Alg {
    /* Iterative Repeat Replacement */


    public String oscType = null;
    public boolean aShift = false;
    public boolean pruneUnused;

    P_OSeqSet usedRhsPatterns = null;
    P_OSeqSet localBlacklist = null;

    public Alg_IRCOO(String oscType, boolean pruneUnused) {
        super();
        this.oscType = oscType;
        this.pruneUnused = pruneUnused;
        this.debug = false;
    }

    private void DUMP_BLACKLIST(Set<OSeq> obj) {
        int i = 0;
        System.out.println(">>>>>");
        for (OSeq s : obj) {
            System.out.println("" + (++i) + ":" + s.toString());
        }
        System.out.println("<<<<<");
    }

    public Grammar process(byte buffer[], boolean verbose) {
        OGrammar g;
        if (MAGR.veryVerbose)
            debug = true;
        if (null != MAGR.loadPrefix) {
            g = (OGrammar) Persistent.load(OGrammar.class, MAGR.loadPrefix);
            usedRhsPatterns = (P_OSeqSet) Persistent.load(P_OSeqSet.class, MAGR.loadPrefix);
        } else {
            g = new OGrammar(true);
            g.addRule(new OSeq(buffer));
            this.usedRhsPatterns = new P_OSeqSet();
            usedRhsPatterns.obj = new TreeSet<OSeq>(new OSeqLengthComparator());
        }
        int a = 0;
        IRCOO_Minimizer ir = null;
        boolean gWasUpdated = true;
        OGST gst = null;
        OOSC oosc = null;
        ONodeInfo ni = null;
        while (true) {
            IRCOO_Minimizer.debug = false;
            a++;
            // if (debug) System.out.println("GRAMMAR:\n g="+g.toString());
            if (debug)
                System.out.println("g.size()=" + g.size());
            // 1. montar Suffix Tree
            if (gWasUpdated) {
                if (null != ni)
                    usedRhsPatterns.obj.add(ni.node.getPattern());
                localBlacklist = usedRhsPatterns.clone();
                gst = g.getYieldGST(aReverse);
                oosc = OOSC.getOSC(oscType, gst, localBlacklist.obj, g);
            }
            // 2. encontrar maior subpadrao
            ni = oosc.selectOcurrence();
            // 3 se nao encontrou, sair
            if (!ni.nonOverlaopingOcurrences)
                break;
            // 4. incluir nova regra na gramatica
            OSeq p = ni.node.getPattern();
            if (debug)
                System.out.println("pattern =" + p.toString());
            if (debug && (-2 == a || -1 == a)) {
                debug = true;
                IRCOO_Minimizer.debug = true;
                DUMP_BLACKLIST(usedRhsPatterns.obj);
            }
            OGrammar g2;
            if (pruneUnused) {
                // In this case minimize grammar based on previous grammar plus new pattern
                g2 = g.clone();
            } else { // !pruneUnused
                // In this case minimize grammar based on ALL previous pattern
                g2 = new OGrammar(true);
                g2.addRule(new OSeq(buffer));
                for (OSeq os : usedRhsPatterns.obj) {
                    g2.addRule(os);
                }
            }
            g2.addRule(p);
            ir = new IRCOO_Minimizer(g2, aReverse);
            // 5. minimiza todos os yields[r];
            ir.ircooMinimizeRhs();
            // 6. Elimina as regras usadas 0 ou 1 vez.
            // if (debug) System.out.println("GRAMMAR:\n min g2="+g2.toString());
            if (IRCOO_Minimizer.debug) {
                System.out.println("g=" + g);
                System.out.println("g2=" + g2);
            }
            OGrammar g3 = ir.renumber();
            if (IRCOO_Minimizer.debug) {
                System.out.println("g3=" + g3);
            }
            // if (debug) System.out.println("GRAMMAR:\n g3="+g3.toString());
            gWasUpdated = false;
            if (debug)
                System.out.println("g.size()=" + g.size());
            if (debug)
                System.out.println("g3.size()=" + g3.size());
            if (g3.size() < g.size()) {
                gWasUpdated = true;
                g = g3;
                if (debug)
                    System.out.println("Grammar updated because g3.size()<g.size()");
            } else {
                if (debug)
                    System.out.println("Grammar not updated because g3.size()>=g.size()");
            }
            if (debug || (verbose && (0 == a % 50))) {
                String pa = p.toString();
                String etc = "";
                int len = pa.length();
                if (len > 10) {
                    len = 10;
                    etc = "...";
                }
                System.out.println("iterations=" + a + "; numrules=" + g.ruleQtt + " pattern =" + pa.substring(0, len)
                        + etc + " pattern.length()=" + p.toString().length());
                System.out.println("localBlacklist.obj.size()=" + localBlacklist.obj.size());
                System.out.println("usedRhsPatterns.obj.size()=" + usedRhsPatterns.obj.size());
            }
            // if (debug) DUMP_BLACKLIST();
            endLoop(a);
            Persistent.conditionalSave(MAGR.saveInterval, g, usedRhsPatterns);
        }
        if (verbose)
            System.out.println("total iterations=" + a);
        if (MAGR.savebinary)
            g.saveBinary();
        return g;
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
    }

}
