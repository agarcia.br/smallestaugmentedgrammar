package tecmf;

import java.io.*;
import java.nio.file.*;
import java.util.Iterator;

import tecmf.smst.OGST;
import tecmf.smst.Grammar;
import tecmf.smst.OGrammar;
import tecmf.smst.ONodeInfo;
import tecmf.smst.OOSC;
import tecmf.smst.OOSC_MC;
import tecmf.smst.OOSC_ML;
import tecmf.smst.Ocurrence;
import tecmf.utils.Persistent;
import tecmf.smst.OSeq;

public class Alg_IRR extends Alg {
    /* Iterative Repeat Replacement */

    public String oscType = null;
    public boolean aShift = false;
    public boolean useRecursiveSemantics = true;

    public Alg_IRR(String oscType) {
        super();
        this.oscType = oscType;
        this.debug=false;
    }

    public Grammar process(byte buffer[], boolean verbose) {
        OGrammar g;
        if (null != MAGR.loadPrefix) {
            g = (OGrammar) Persistent.load(OGrammar.class, MAGR.loadPrefix);
        } else {
            g = new OGrammar(useRecursiveSemantics);
            g.addRule(new OSeq(buffer));
        }
        int a = 0;
        while (true) {
            a++;
            if (debug)
                System.out.println("GRAMMAR:");
            if (debug)
                System.out.println(g.toString());
            // 1. montar Suffix Tree
            OGST gst = g.getRhsGST(aComplement, aReverse, useRecursiveSemantics);
            // 2. encontrar maior subpadrao
            ONodeInfo ni = (OOSC.getOSC(oscType, gst, null, g)).selectOcurrence();
            // 3 se nao encontrou, sair
            if (!ni.nonOverlaopingOcurrences)
                break;
            // 4. incluir nova regra na gramatica
            OSeq p = ni.node.getPattern();
            Integer length = p.length();
            g.addRule(p);
            // 5. substituir padrao nas regras
            Iterator<Ocurrence> it = ni.ocurrences.iterator();
            int oldrule = -1;
            int ruleTotalRhsLenBeforeChange = -1;
            while (it.hasNext()) {
                /*
                 * 1. Must be iterator to assure left to right scan 2. Since a single rule may
                 * change more times, its length must be kept to calculatre adjustedLeftPos
                 */
                Ocurrence oc = it.next();
                int rule = oc.nonTerminal.getValue();
                if (rule != oldrule) {
                    oldrule = rule;
                    ruleTotalRhsLenBeforeChange = g.rule(rule).length();
                }
                if (debug)
                    System.out.print("oc=<" + oc.toString() + ">;");
                ((OSeq) g.rules.get(oc.nonTerminal.getValue())).reduce(oc, length, (short) (g.ruleQtt - 1),
                        ruleTotalRhsLenBeforeChange);
            }
            endLoop(a);
            Persistent.conditionalSave(MAGR.saveInterval, g);
        }
        if (verbose)
            System.out.println("total iterations=" + a);
        if (MAGR.savebinary)
            g.saveBinary();
        return g;
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
    }

}
