package tecmf.smst;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

import tecmf.utils.Persistent;

/* Short Sequence is similar to Strings, but allows rules references besides chars */
public class ASymbol extends Persistent implements Comparable {

    private static final long serialVersionUID = 8688705530567954011L;
    private short value;
    private boolean isUniqueTerminator;
    private boolean isNonterminal;
    private boolean isReversed;
    private boolean isComplemented;

    public ASymbol(short value) {
        this.value = value;
    }

    public ASymbol(byte value) {
        if (value >= 0) {
            this.value = (short) value;
        } else {
            this.value = (short) (value+256);
        }
    }

    
    public ASymbol(short value, boolean isUniqueTerminator, boolean isNonterminal, boolean isReversed, boolean isComplemented) {
        this.value = value;
        this.isUniqueTerminator = isUniqueTerminator;
        this.isNonterminal = isNonterminal;
        this.isReversed = isReversed;
        this.isComplemented = isComplemented;
    }

    static public ASymbol nonTerminal(short e) {
        return new ASymbol(e, false, true, false, false);
    }

    static public ASymbol terminator(short e) {
        return new ASymbol(e, true, false, false, false);
    }

    static public ASymbol nonTerminalFromTerminator(ASymbol t) {
        assert (t.isUniqueTerminator());
        return new ASymbol(t.getValue(), false, true, t.isReversed(), t.isComplemented() );
    }

    public ASymbol clone() {
        ASymbol ret = new ASymbol(getValue(), isUniqueTerminator(), isNonterminal(), isReversed(), isComplemented());
        return ret;
    }

    static public ASymbol reverseTerminatorFromTerminator(ASymbol t) {
        assert (t.isUniqueTerminator());
        return new ASymbol(t.getValue(), true, false, !t.isReversed(), t.isComplemented());
    }

    static public ASymbol reverseNonTerminalFromNonterminal(ASymbol t) {
        assert (t.isNonterminal());
        return new ASymbol(t.getValue(), false, true, !t.isReversed(), t.isComplemented());
    }

    static public ASymbol fromShortCode(short r) {
        boolean isUniqueTerminator = (r < 0);
        if (isUniqueTerminator) {
            r = (short) (-r - OGrammar.IRR);
        }
        boolean isNonterminal = (r >= OGrammar.IRR);
        if (isNonterminal) {
            r = (short) (r - OGrammar.IRR);
        }
        boolean isReversed = false;
        boolean isComplemented = false;
        ASymbol ret = new ASymbol(r, isUniqueTerminator, isNonterminal, isReversed, isComplemented);
        return ret;
    }

    public boolean equals(Object s) {
        boolean ret = false;
        if (s instanceof ASymbol) {
            ASymbol os = (ASymbol) s;
            ret = (os.value == value) && (os.isUniqueTerminator == isUniqueTerminator)
                    && (os.isNonterminal == isNonterminal) && (os.isReversed == isReversed) && (os.isComplemented == isComplemented);
        }
        // System.err.println(""+toString()+".equals("+s.toString()+")="+ret);
        return ret;
    }

    public int encodeSize() {
        if (isNonterminal()) {
            return ("" + value).length();
        } else {
            return 1;
        }
    }

    public boolean isToPrint() {
        // 38='&' 60='<'
        return value >= 32 && value <= 126 && value != 36 && value != 60;
    }

    public String toString() {
        assert (value >= 0);
        StringBuilder b = new StringBuilder();
        if (isUniqueTerminator()) {
            b.append('$');
        }
        if (isNonterminal())
            b.append('<');
        if (isTerminal() && isToPrint()) {
            b.append((char) value);
        } else {
            if (isTerminal())
                b.append("&#");
            b.append(value);
            if (isTerminal())
                b.append(";");
        }
        if (isNonterminal())
            b.append('>');
        if (isReversed() | isComplemented()) {
            b.append("^{");
            if (isReversed())
                b.append('R');
            if (isComplemented()) {
                b.append('C');
            }
            assert (isNonterminal() || isUniqueTerminator());
            b.append('}');
        }
        return b.toString();
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public boolean isUniqueTerminator() {
        return isUniqueTerminator;
    }

    public void setUniqueTerminator(boolean isUniqueTerminator) {
        this.isUniqueTerminator = isUniqueTerminator;
    }

    public boolean isNonterminal() {
        return isNonterminal;
    }

    public boolean isTerminal() {
        return !isNonterminal && !isUniqueTerminator;
    }

    public void setNonterminal(boolean isNonterminal) {
        this.isNonterminal = isNonterminal;
    }

    public boolean isReversed() {
        return isReversed;
    }

    public void setReversed(boolean isReversed) {
        this.isReversed = isReversed;
    }

    public boolean isComplemented() {
        return isComplemented;
    }

    public void setComplemented(boolean isComplemented) {
        this.isComplemented = isComplemented;
    }

    /*
    public ASymbol complement() {
        assert(!isTerminal());
        isComplemented = !isComplemented;
        return this;
    }*/ 

    
    public int compareTo(Object o) {
        int ret = 0;
        if (o instanceof ASymbol) {
            ASymbol oc = (ASymbol) o;
            if (!isNonterminal() && oc.isNonterminal()) {
                ret = -1;
            } else if (isNonterminal() && !oc.isNonterminal()) {
                ret = 1;
            } else if (!isUniqueTerminator() && oc.isUniqueTerminator()) {
                ret = -1;
            } else if (isUniqueTerminator() && !oc.isUniqueTerminator()) {
                ret = 1;
            } else if (getValue() < oc.getValue()) {
                ret = -1;
            } else if (getValue() > oc.getValue()) {
                ret = 1;
            } else if (!isReversed() && oc.isReversed()) {
                ret = -1;
            } else if (isReversed() && !oc.isReversed()) {
                ret = 1;
            } else if (!isComplemented() && oc.isComplemented()) {
                ret = -1;
            } else if (isComplemented() && !oc.isComplemented()) {
                ret = 1;
            }
        }
        // System.err.println(""+toString()+".compareTo("+o.toString()+")="+ret);
        return ret;
    }

    public static void serializationTest() {
        ASymbol a = ASymbol.nonTerminal((short) 1);
        a.setReversed(true);
        System.out.println("a=" + a.toString());
        a.save("testeA");
        ASymbol a2 = (ASymbol) Persistent.load(ASymbol.class, "testeA");
        System.out.println("a2=" + a2.toString());
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        serializationTest();
    }

}
