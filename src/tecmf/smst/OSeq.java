package tecmf.smst;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import tecmf.MAGR;
import tecmf.utils.Persistent;

/* Short Sequence is similar to Strings, but allows rules references besides chars */
public class OSeq extends AString {

    private static final long serialVersionUID = -4622286670472938110L;

    private boolean debug = false;

    public ASymbol seq[];

    public OSeq() {
        this.seq = new ASymbol[0];
    }

    public OSeq(ASymbol seq[]) {
        this.seq = seq;
    }

    public OSeq(ASymbol s[], int len) {
        seq = new ASymbol[len];
        // System.arraycopy(src, srcPos, dest, destPos, length);
        System.arraycopy(s, 0, seq, 0, len);
    }

    public OSeq(short s[], int len) {
        seq = new ASymbol[len];
        for (int i = 0; i < len; ++i) {
            seq[i] = ASymbol.fromShortCode(s[i]);
        }
    }

    public OSeq(short s[]) {
        seq = new ASymbol[s.length];
        for (int i = 0; i < s.length; ++i) {
            seq[i] = new ASymbol(s[i]);
        }
    }

    public OSeq(byte ba[]) {
        seq = new ASymbol[ba.length];
        for (int i = 0; i < ba.length; ++i) {
            seq[i] = new ASymbol(ba[i]);
        }
    }

    public OSeq(String s) {
        seq = new ASymbol[s.length()];
        for (int i = 0; i < s.length(); ++i) {
            seq[i] = new ASymbol((short) s.charAt(i));
        }
    }

    public void writeToStream(DataOutputStream dos) throws IOException {
        // 0x00 will be scaped as //0x00 0x00
        // Nonterminals will be scaped as //0x00 0x01 <short>
        // Rversed Nonterminals will be scaped as //0x00 0x02 <short>
        // so breaks can be coded as //0x00 0x0A
        for (int i = 0; i < seq.length; ++i) {
            if (seq[i].isTerminal()) {
                if (0x00 == seq[i].getValue()) {
                    dos.writeByte(0x00);
                }
                dos.writeByte(seq[i].getValue());
            } else if (seq[i].isNonterminal()) {
                dos.writeByte(0x00);
                if (seq[i].isReversed()) {
                    dos.writeByte(0x01);
                } else {
                    dos.writeByte(0x02);
                }
                dos.writeShort(seq[i].getValue());
            } else {
                // ignore uniqueTerminators
            }
        }
    }

    public int encodeSize() {
        int size = 0;
        for (int i = 0; i < seq.length; ++i) {
            size += seq[i].encodeSize();
        }
        return size;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < seq.length; ++i) {
            sb.append(seq[i].toString());
        }
        return sb.toString();

    }

    @Override 
    public boolean equals(Object s) {
        if (null == s) return false;
        if (getClass()!=s.getClass()) return false;
        //boolean ret = Arrays.equals(seq, s.seq);
        boolean ret = this.toString().equals(s.toString());
        if (debug)
            System.out.println("OSeq.equals()=" + ret);
        return ret;
    }

    @Override 
    public int hashCode() {
        int hash = this.toString().hashCode();
        if (debug)
            System.out.println("SSeq.hashCode()=" + hash);
        return hash;
    }

    public OSeq clone() {
        ASymbol seq2[] = new ASymbol[seq.length];
        System.arraycopy(seq, 0, seq2, 0, seq.length);
        return new OSeq(seq2);
    }

    public OSeq complement() {
        ASymbol seq2[] = new ASymbol[seq.length];
        for (int i=0;i<seq2.length;++i) {
            seq2[i]=MAGR.complement.comp(seq[i].clone());
        }
        return new OSeq(seq2);
    }

    public OSeq reverseRhs(boolean useRecursiveSemantics) {
        ASymbol seq2[] = new ASymbol[seq.length];
        int lenWithNoTerminator = seq.length - 1;
        for (int i = 0; i < lenWithNoTerminator; ++i) {
            seq2[lenWithNoTerminator - 1 - i] = seq[i].clone();
            if (useRecursiveSemantics && seq2[lenWithNoTerminator - 1 - i].isNonterminal()) {
                seq2[lenWithNoTerminator - 1 - i].setReversed(!seq2[lenWithNoTerminator - 1 - i].isReversed());
            }
        }
        ASymbol oldTerminator = seq[seq.length - 1];
        assert (oldTerminator.isUniqueTerminator());
        assert (!oldTerminator.isReversed());
        ASymbol newTerminator = ASymbol.reverseTerminatorFromTerminator(oldTerminator);
        assert (newTerminator.isReversed());
        seq2[seq.length - 1] = newTerminator;
        return new OSeq(seq2);
    }

    public OSeq reverse(boolean recursiveSemantics) {
        ASymbol seq2[] = new ASymbol[seq.length];
        for (int i = 0; i < seq.length; ++i) {
            seq2[i] = seq[seq.length - i - 1].clone();
            if (recursiveSemantics && seq2[i].isNonterminal()) {
                seq2[i].setReversed(!seq2[i].isReversed());
            }
        }
        return new OSeq(seq2);
    }
    
    public OSeq append(short s) {
        ASymbol seq2[] = new ASymbol[seq.length + 1];
        System.arraycopy(seq, 0, seq2, 0, seq.length);
        seq2[seq.length] = new ASymbol(s);
        seq = seq2;
        return this;
    }

    public OSeq append(ASymbol s) {
        ASymbol seq2[] = new ASymbol[seq.length + 1];
        System.arraycopy(seq, 0, seq2, 0, seq.length);
        seq2[seq.length] = s;
        seq = seq2;
        return this;
    }

    public OSeq reduce(Ocurrence o, int len, short rule, int ruleTotalRhsLenBeforeChange) {
        ASymbol a = ASymbol.nonTerminal(rule);
        a.setReversed(o.nonTerminal.isReversed());
        a.setComplemented(o.nonTerminal.isComplemented());
        int alp = Ocurrence.adjustedLeftPos(o, ruleTotalRhsLenBeforeChange, len);
//System.err.println("alp="+alp);
        return reduce(alp, len, a);
    }

    public OSeq reduce(int adjustedLeft, int len, ASymbol newSymbolToChange) {
        ASymbol seq2[] = new ASymbol[seq.length - len + 1];
        if (0 < adjustedLeft) {
            System.arraycopy(seq, 0, seq2, 0, adjustedLeft);
        }
        seq2[adjustedLeft] = newSymbolToChange;
        System.arraycopy(seq, adjustedLeft + len, seq2, adjustedLeft + 1, seq2.length - adjustedLeft - 1);
        seq = seq2;
        return this;
    }

    public OSeq shiftRight(short n) {
        assert (n >= 0);
        n = (short) (n % seq.length);
        ASymbol seq2[] = new ASymbol[seq.length];
        for (int i = 0; i < seq.length; ++i) {
            seq2[i] = seq[(i + seq.length - n) % seq.length];
        }
        return new OSeq(seq2);
    }

    public OSeq expandRule(short ruleNumber, OSeq rhs, boolean hasTerminator, boolean recursiveSemantics) {
        //System.out.println("expandRule ruleNumber="+ruleNumber+ " rhs="+rhs.toString());
        return expandRule(ruleNumber, rhs, hasTerminator, recursiveSemantics, null);
    }

    public OSeq expandRule(short ruleNumber, OSeq rhs, boolean hasTerminator, boolean recursiveSemantics, Grammar g) {
        // aqui � mais complicado, porque tem que fazer o reverso e o shift
        // Nao levar em conta o ultimo short do rhs
        String s1 = null;
        String s2 = null;
        if (null != g)
            s1 = g.toString();
        OSeq rhsNoTerminator = hasTerminator ? rhs.subseq(0, rhs.length() - 1) : rhs;
        OSeq build = new OSeq();
        //System.out.println("this="+this.toString());
        for (int i = 0; i < seq.length; ++i) {
            //System.out.println("seq["+i+"]="+seq[i].toString());
            //System.out.println("build=*"+build.toString()+"*");
            //System.out.println("seq[i].isNonterminal()="+seq[i].isNonterminal());
            //System.out.println("seq[i].getValue()="+seq[i].getValue());
            //System.out.println("ruleNumber="+ruleNumber);
            if (seq[i].isNonterminal() && (seq[i].getValue() == ruleNumber)) {
                if (seq[i].isComplemented()) {
                    //System.out.println("seq["+i+"].isComplemented()");
                    if (seq[i].isReversed()) {
                        build.append((OSeq) MAGR.complement.comp(rhsNoTerminator.reverse(recursiveSemantics)));
                    } else {
                        build.append((OSeq) MAGR.complement.comp(rhsNoTerminator));
                    }
                } else {
                    if (seq[i].isReversed()) {
                        build.append(rhsNoTerminator.reverse(recursiveSemantics));
                    } else {
                        build.append(rhsNoTerminator);
                    }
                }
                /*
                 * if (seq[i].getShiftValue()!=0) { replace =
                 * rhsNoTerminator.shiftRight(seq[i].getShiftValue()); }
                 */
            } else {
                build.append(seq[i]);
            }
        }
        seq = build.seq;
        return this;
    }

    public void remove(int pos) {
        ASymbol seq2[] = new ASymbol[seq.length -1];
        if (pos>0) {
            System.arraycopy(seq, 0, seq2, 0, pos);
        }
        if (seq.length-pos-1>0) {
            System.arraycopy(seq, pos+1, seq2, pos, seq.length-pos-1);
        }
        seq = seq2;
    }

    public void expandRuleLocal(int pos, OSeq rhsInserted, boolean hasTerminator, boolean recursiveSemantics) {
        OSeq rhsNoTerminator = hasTerminator ? rhsInserted.subseq(0, rhsInserted.length() - 1) : rhsInserted;
        OSeq build = new OSeq();
        if (pos>0) {
            build.append(this.subseq(0, pos));
        }
        assert (seq[pos].isNonterminal());
        if (seq[pos].isComplemented()) {
            if (seq[pos].isReversed()) {
                build.append((OSeq) MAGR.complement.comp(rhsNoTerminator.reverse(recursiveSemantics)));
            } else {
                build.append((OSeq) MAGR.complement.comp(rhsNoTerminator));
            }
        } else {
            if (seq[pos].isReversed()) {
                build.append(rhsNoTerminator.reverse(recursiveSemantics));
            } else {
                build.append(rhsNoTerminator);
            }
        }
        if (seq.length-pos-1>0) {
            build.append(this.subseq(pos+1));
        }
        seq = build.seq;
    }

    public OSeq append(OSeq s) {
        ASymbol seq2[] = new ASymbol[seq.length + s.seq.length];
        System.arraycopy(seq, 0, seq2, 0, seq.length);
        System.arraycopy(s.seq, 0, seq2, seq.length, s.seq.length);
        seq = seq2;
        return this;
    }

    public OSeq cloneAppend(OSeq s) {
        ASymbol seq2[] = new ASymbol[seq.length + s.seq.length];
        System.arraycopy(seq, 0, seq2, 0, seq.length);
        System.arraycopy(s.seq, 0, seq2, seq.length, s.seq.length);
        return new OSeq(seq2);
    }

    public OSeq preppend(OSeq s) {
        ASymbol seq2[] = new ASymbol[seq.length + s.seq.length];
        System.arraycopy(s.seq, 0, seq2, 0, s.seq.length);
        System.arraycopy(seq, 0, seq2, s.seq.length, seq.length);
        seq = seq2;
        return this;
    }

    public int length() {
        return seq.length;
    }

    public OSeq subseq(int beginindex, int endindex) {
        int len = endindex - beginindex;
        ASymbol seq2[] = new ASymbol[len];
        System.arraycopy(seq, beginindex, seq2, 0, len);
        return new OSeq(seq2);
    }

    public int findPairFirstOcurrence(OSeq p, boolean areverse, boolean acomplement) {
        assert(2==p.length());
        OSeq rev = null;
        OSeq comp = null;
        OSeq revcomp = null;
        if (areverse) {
            rev = p.reverse(true);
        }
        if (acomplement) {
            comp = p.complement();
        }
        if (areverse && acomplement) {
            revcomp = rev.complement();
        }

        for (int i=0; i<seq.length-1;++i) {
            if (seq[i].equals(p.seq[0])&&seq[i+1].equals(p.seq[1])) return i;
            if (areverse) {
                if (seq[i].equals(rev.seq[0])&&seq[i+1].equals(rev.seq[1])) return i;
            }
            if (acomplement) {
                if (seq[i].equals(comp.seq[0])&&seq[i+1].equals(comp.seq[1])) return i;
            }
            if (areverse && acomplement) {
                if (seq[i].equals(revcomp.seq[0])&&seq[i+1].equals(revcomp.seq[1])) return i;
            }
        }
        return -1;
    }

    public OSeq subseq(int beginindex) {
        return subseq(beginindex, length());
    }

    public boolean startsWith(OSeq b) {
        if (length() < b.length())
            return false;
        return Arrays.equals(seq, 0, b.length(), b.seq, 0, b.length());
    }

    public ASymbol get(int i) {
        return seq[i];
    }

    public ASymbol getFirst() {
        return seq[0];
    }

    public ASymbol getLastNonTerminator() {
        ASymbol ret = seq[seq.length - 1];
        if (ret.isUniqueTerminator()) {
            if (seq.length > 1) {
                return seq[seq.length - 2];
            } else {
                return null;
            }
        }
        return ret;
    }

    public static void serializationTest() {
        OSeq s = new OSeq("teste");
        s.append(ASymbol.terminator((short) 0));
        System.out.println("s=" + s.toString());
        s.save("testeOSeq2");
        OSeq s2 = (OSeq) Persistent.load(OSeq.class, "testeOSeq2");
        System.out.println("s2=" + s2.toString());
    }

    public static void main(String args[]) {
        serializationTest();
        OSeq s = new OSeq("teste");
        System.out.println("s.reverse()=" + s.reverse(false));
        for (short i = 0; i < 5; ++i)
            System.out.println("+s.shiftRight(" + i + ")=" + s.shiftRight(i));
        OSeq s2 = s.clone();
        s2.seq[4] = new ASymbol((short) 'a');
        assert (!s.equals(s2));
        OSeq s3 = new OSeq("testa");
        assert (s3.equals(s2));
        OSeq s4 = s2.cloneAppend(s);
        OSeq s5 = s2.clone().append(s);
        assert (s4.equals(s5));
        System.out.println("s4=" + s4);
        // (value, isUniqueTerminator, isNonterminal, isReversed, isComplemented)
        ASymbol r1 = new ASymbol((short) 1, false, true, false, false);
        ASymbol r1Reversed = new ASymbol((short) 1, false, true, true, false);
        OSeq subst = new OSeq().append(r1).append(r1Reversed);
        OSeq exp = subst.expandRule((short) 1, s, false, false);
        System.out.println("exp=" + exp);

    }

}
