package tecmf.smst;

import java.io.*;
import java.util.*;

import tecmf.MAGR;

import tecmf.ircoo.YieldIter;
import tecmf.utils.*;

public class OGrammar extends Grammar {

    private static final long serialVersionUID = 414065702691092951L;

    public AString rule(int i) {
        return rules.elementAt(i);
    }

    public OGrammar(boolean recursiveSemantics) {
        super();
        this.recursiveSemantics = recursiveSemantics;
    }

    public OSeq yieldOSeq(short r, boolean showTerminator) {
        Vector<ASymbol> v = new Vector<ASymbol>();
        YieldIter y = new YieldIter(this, r, showTerminator, false);
        while (y.hasNext()) {
            v.add(y.next());
        }
        ASymbol[] v2 = v.toArray(new ASymbol[0]);
        return new OSeq(v2);
    }

    public void addRule(String s) {
        addRule(new OSeq(s));
    }

    public OGrammar clone() {
        OGrammar ret = new OGrammar(recursiveSemantics);
        for (int i = 0; i < rules.size(); ++i) {
            ret.addRule((OSeq) rules.get(i));
        }
        return ret;
    }

    public short addRule(OSeq s) {
        assert (rules.size() == ruleQtt);
        OSeq s2 = s.clone();
        if (!s2.seq[s2.length() - 1].isUniqueTerminator()) {
            s2.append(ASymbol.terminator(ruleQtt));
        }
        rules.add(s2);
        return ruleQtt++;
    }

    public void setRule(short index, OSeq s) {
        assert (rules.size() == ruleQtt);
        rules.set(index, s.clone().append(ASymbol.terminator(index)));
    }

    public OGST getYieldGST(boolean aReverse) {
        OGST gst = new OGST();
        for (short i = 0; i < rules.size(); ++i) {
            OSeq r = null;
            r = yieldOSeq(i, true);
            assert (null != r);
            gst.put(r);
            if (aReverse) {
                OSeq r2 = r.reverseRhs(false);
                gst.put(r2);
            }
        }
        return gst;
    }

    public OGST getRhsGST(boolean aComplement, boolean aReverse, boolean useRecursiveSemantics) {
        OGST gst = new OGST();
        for (int i = 0; i < rules.size(); ++i) {
            OSeq r = (OSeq) rules.get(i);
            assert (null != r);
            gst.put(r);
            if (aReverse) {
                OSeq r2 = r.reverseRhs(useRecursiveSemantics);
                gst.put(r2);
            }
            if (aComplement) {
                OSeq r2 = r.complement();
//System.out.println("r2 ="+r2.toString());                
                gst.put(r2);
            }
            if (aComplement&&aReverse) {
                OSeq r2 = r.reverseRhs(useRecursiveSemantics).complement();
                gst.put(r2);
            }
        }
        return gst;
    }


    public OSeq oldGenerateString() {
        OSeq expandedrules[] = new OSeq[rules.size()];
        for (short i = 0; i < rules.size(); ++i) {
            expandedrules[i] = ((OSeq) rules.get(i)).subseq(0, rules.get(i).length() - 1);
            ;
        }
        for (short ruleToSubstitute = (short) (rules.size() - 1); ruleToSubstitute > 0; --ruleToSubstitute) {
            if (MAGR.verbose)  System.out.println("ruleToSubstitute=" + ruleToSubstitute);
            for (short j = 0; j < ruleToSubstitute; ++j) {
                if (MAGR.verbose &&0==(j%200))  System.out.print(";"+j);
                String s1 = this.toString();
                expandedrules[j].expandRule((short) (ruleToSubstitute), expandedrules[ruleToSubstitute], false,
                        recursiveSemantics);
                String s2 = this.toString();
                if (!s1.contentEquals(s2)) {
                    System.out.println("************ !s1.contentEquals(s2) j=" + j);
                    System.out.println("s1=" + s1);
                    System.out.println("s2=" + s2);
                }
            }
            if (MAGR.verbose)  System.out.println("!");
        }
        return expandedrules[0];
    }

    public OSeq generateString() {
        Set<Short> ruleGraph[] = new Set[rules.size()];
        for (short i = 0; i < rules.size(); ++i) {
            ruleGraph[i] = new HashSet<Short>();
            OSeq r = (OSeq) rule(i);
            for (short j = 0; j < r.length()-1; ++j) {
                if (r.seq[j].isNonterminal()) {
                    ruleGraph[i].add(r.seq[j].getValue());
                }
            }
        }
        DFS dfs = new DFS(ruleGraph);
        dfs.dfs();
        Deque<Short> sort = dfs.sort;
        short ruleOrder[] = new short[rules.size()];
        short k=0;
        while (!sort.isEmpty()) {
            short r =  sort.removeFirst();
            ruleOrder[k] = r;
            //reverseRuleOrder[r] = k;
            k++;
        }
        /*
        if (k!=rules.size()) {
            System.out.println("k="+k);
            System.out.println("rules.size()="+rules.size());
            for (short i = 0; i < k; ++i) {
                System.out.println("ruleOrder["+i+"]="+ruleOrder[i]);
            }
            System.out.println("g="+this);
        }*/
        assert(k<=rules.size());
        OSeq expandedrules[] = new OSeq[rules.size()];
        for (short i = 0; i < rules.size(); ++i) {
            expandedrules[i] = ((OSeq) rules.get(i)).subseq(0, rules.get(i).length() - 1);
        }
        for (short i = 0; i < k /*rules.size()*/; ++i) {
            short ruleToSubstitute = ruleOrder[i];
            for (short j = (short)(i+1); j <k /*rules.size()*/; ++j) {
                short substitutingRule = ruleOrder[j];
                String s1 = this.toString();
                expandedrules[substitutingRule].expandRule((short) (ruleToSubstitute), expandedrules[ruleToSubstitute], false,
                        recursiveSemantics);
                String s2 = this.toString();
                if (!s1.contentEquals(s2)) {
                    System.out.println("************ !s1.contentEquals(s2) j=" + j);
                    System.out.println("************ !s1.contentEquals(s2) substitutingRule=" + substitutingRule);
                    System.out.println("s1=" + s1);
                    System.out.println("s2=" + s2);
                }
            }
        }
        return expandedrules[0];
    }

    public static void serializationTest() {
        System.setProperty("sun.io.serialization.extendedDebugInfo", "true");
        OGrammar g = new OGrammar(true);
        g.addRule("abcdefg");
        g.addRule("cd");
        ((OSeq) g.rule(0)).reduce(2, 2, ASymbol.nonTerminal((short) 1));
        System.out.println("g=" + g.toString());
        g.save("testeOG");
        OGrammar g2 = (OGrammar) Persistent.load(OGrammar.class, "testeOG");
        System.out.println("g2=" + g2.toString());
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        //serializationTest();
    }
}
