package tecmf.smst;

import tecmf.*;

import java.util.*;

public class OOSC_MC extends OOSC {
    /* Ocurrence Selection Criteria :(l)ength */

    boolean breakTieFavorsLonger = true;
    
    public OOSC_MC(OGST gst, Set<OSeq> blacklist, Grammar g) {
        super(gst, blacklist, g);
        fillMatchesAndScore(gst.root);
        queueNodes(gst.root);
    }

    protected int getScore(ONode n) {
        int o = n.filteredNonOverlapingMatches.size();
        int alen = n.length;
        n.score = reducesGrammar(n) ? o * alen - o - alen : 0;
        return n.score;
    }

    private Comparator<ONode> comparator;
    private PriorityQueue<ONode> queue;

    protected void queueNodes(ONode root) {
        comparator = new ONodeCompactComparator(breakTieFavorsLonger);
        queue = new PriorityQueue<ONode>(100, comparator);
        OGSTVisitor v = new OGSTVisitor(OGSTVisitor.DEPTH_FIRST_POST_ORDER) {
            public void visit(ONode n) {
                if (!n.arrows.isEmpty()) {
                    // Internal Nodes Only
                    if (2 > n.length)
                        return; // can not be used
                    queue.add(n);
                }
            }
        };
        v.traverse(root);
    }

    protected ONodeInfo selectOcurrence(OSeq path, ONode n) {

        ONode choosenSubNode = null;
        while (queue.size() != 0) {
            ONode subNode = queue.remove();
            if (subNode.score <= 0) {
                break; // nothing else to look for
            }
            if (subNode.id == 23) {
                System.out.println("23");
            }
            int count = subNode.filteredNonOverlapingMatches.size();
            // int count2 = fillNonOverlappingMatches(subNode);
            assert (count > 1);
            // assert(count==count2);
            if (debug) {
                System.out.println("subNode=" + subNode);
                System.out.println(">>count=" + count);
                // System.out.println(">>count2="+count2);
            }
            if (blacklisted(subNode))
                count = 0;
            if (2 < count || (2 == count && subNode.length > 2)) {
                choosenSubNode = subNode;
                break;
            }
        }
        if (debug) {
            System.out.println("choosenSubNode=" + choosenSubNode);
            if (null != choosenSubNode)
                System.out.println("choosenSubNode.score=" + choosenSubNode.score);
        }
        ONodeInfo ni = createNodeInfo(choosenSubNode);
        blacklist(ni.node);
        return ni;
    }

    public static void main(String args[]) {
        test("MC", "xax", 7, null, false, "xaxbxcxAxbxcxaxBxcxaxbxCxaxcxbxDxbxaxcxExcxbxaxFxaxGxbxHxcx");
        test("MC", "xax", 3, null,false, "xaxbxcxAxbxcxaxBxcxaxbxC");
        // Os abaixo comentados s�o empate com o de cima
        // test("MC", "xaxbx", 2, false,false, "xaxbxcxAxbxcxaxBxcxaxbxC");
        // test("MC", "xbx", 3, false,false, "xaxbxcxAxbxcxaxBxcxaxbxC");
        // test("MC", "xcx", 3, false,false, "xaxbxcxAxbxcxaxBxcxaxbxC");
        test("MC", "xax", 7, null, false, "xaxbxcxAxbxcxaxBxcxaxbxCxaxcxbxDxbxaxcxExcxbxaxFxaxGxbxHxcx");
        test("MC", "xaxbx", 2, null,false, "xaxbxcxAxZxZxZxBxcxaxbxC");
        test("MC", "xax", 3, null,false, "xaxxAxxaxBxaxC");
        test("MC", "abccba", 2, null, false,"abccba", "abccba");
        test("MC", "abc", 2, null, true, "abcdef", "XcbaY");
        test("MC", "abcd", 3, null, false, "abcdabcdabcd");
        test("MC", "ab", 4, null, true, "abcdef", "XcbaYbaZba");
        test("MC", "abcdef", 2, null, true, "qwabcdef", "XcbaYfedcbaZba");
        test("MC", "", 0, null,true, "xax"); // as ocorrencias de xax se sobrep�e
        test("MC", "", 0, null,true, "xaAax"); // as ocorrencias de padr�o maior que 3 se sobrep�e
        test("MC", "bax", 2, null,true, "xabAbaxB"); // as ocorrencias maiores que bax se sobrep�e
        test("MC", "bax", 2, null,true, "xabbax"); // as ocorrencias maiores que bax se sobrep�e
        test("MC", "xaxbx", 2, null, false,"xaxbxAxaxbxB");
        test("MC", "xaxx", 2, null,true, "xxaxxaxx");
        test("MC", "xax", 4, null,false, "xaxxAYxxaxB", "BxaxxYAxxax");// caso 0
        test("MC", "xbxax", 2, null,true, "xaxbxAxbxaxB");// caso 0
        test("MC", "xbxax", 2, null,true, "xaxbxAxbxax");
        test("MC", "xxax", 2, null,true, "xaxxAxxax");
        test("MC", "xxax", 2, null,true, "xaxxxxax"); /* A */
        test("MC", "xaxxxxax", 2, null,false, "xaxxxxax", "xaxxxxax"); /* B */

        test("MC", "bcc", 2, "B", false, "bccZcbb");
        test("MC", "bb", 4, "B", false, "bccZcbbRbbcYccb");
        test("MC", "bb", 4, null, false, "bccZcbbRbbcYccb","cbb[bccSccbXbbc");
        test("MC", "atcg", 2, "D", false, "atcggtagc");
        test("MC", "bbc", 4, "B", true, "bccZcbbRbbcYccb");
        test("MC", "abc", 4, null, true, "xyxyxyxyxyabcabcabcabc");

        System.out.println("end tests");
    }

}
