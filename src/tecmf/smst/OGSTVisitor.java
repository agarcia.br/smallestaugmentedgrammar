package tecmf.smst;

abstract public class OGSTVisitor {

    static public final int DEPTH_FIRST_POST_ORDER = 0;

    int order;

    OGSTVisitor(int order) {
        this.order = order;
    }

    public void visit(OGST t) {
        traverse(t.root);
    }

    public void traverse(ONode n) {
        for (OArrow e : n.arrows.values()) {
            ONode n1 = e.dest;
            if (null != n1)
                traverse(n1);
        }
        if (DEPTH_FIRST_POST_ORDER == order)
            visit(n);
    }

    public void traverse(ONode n, int level) {
        for (OArrow e : n.arrows.values()) {
            ONode n1 = e.dest;
            if (null != n1)
                traverse(n1, level + 1);
        }
        if (DEPTH_FIRST_POST_ORDER == order)
            visit(n, level);
    }

    abstract public void visit(ONode n);

    public void visit(ONode n, int level) {
    }

}
