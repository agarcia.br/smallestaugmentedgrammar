package tecmf.smst;

import tecmf.*;

import java.util.*;

//StringLengthComparator.java
import java.util.Comparator;

public class ONodeLengthComparator implements Comparator<ONode> {
    @Override
    public int compare(ONode x, ONode y) {
        return y.length - x.length;
    }
}