package tecmf.smst;

import java.util.*;

import tecmf.MAGR;
import tecmf.gene.BinaryComplement;
import tecmf.gene.DNAComplement;
import tecmf.gene.RNAComplement;
import tecmf.utils.P_OSeqSet;

abstract public class OOSC {
    /* Ocurrence Selection Criteria */
    OGST gst;
    Grammar g; // used only in case of GST with reverse to read rhs length

    protected static boolean debug = false;
    protected Set<OSeq> blacklist = null;

    public OOSC(OGST gst, Set<OSeq> blacklist, Grammar g) {
        assert (gst != null);
        assert (g != null || !gst.hasReverse);
        this.g = g;
        this.blacklist = blacklist;
        this.gst = gst;
    }

    public ONodeInfo selectOcurrence() {
        return selectOcurrence(new OSeq(), gst.root);
    }

    protected int getValidScore(ONodeInfo ni) {
        return ni.nonOverlaopingOcurrences ? getScore(ni.node) : 0;
    }

    protected boolean blacklisted(ONode subNode) {
        if (null == blacklist)
            return false;
        return blacklist.contains(subNode.getPattern());
    }

    protected void blacklist(ONode subNode) {
        if (null != blacklist && null != subNode)
            blacklist.add(subNode.getPattern());
    }

    abstract protected int getScore(ONode n);

    protected void fillMatchesAndScore(ONode root) {
        OGSTVisitor v = new OGSTVisitor(OGSTVisitor.DEPTH_FIRST_POST_ORDER) {
            public void visit(ONode n) {
                if (!n.arrows.isEmpty()) {
                    if (1 >= n.length)
                        return; // n.matches not used
                    /* Internal Node */
                    assert (0 == n.matches.size());
                    for (OArrow e : n.arrows.values()) {
                        n.matches.addAll(e.dest.matches);
                    }
                    if ("bb".contentEquals(n.getPattern().toString())) {
                        debug=false;
                    } else {
                        debug=false;
                    }
                    fillNonOverlappingMatches(n);
                    /* n.score = */
                    getScore(n);
                }
            }
        };
        v.traverse(root);
    }

    protected int fillNonOverlappingMatches(ONode n) {
//		int patternlen, TreeSet<Ocurrence> matches
        int count = 0;
        short lastRule = -1;
        int lastLeftPostion = -1;
        //if (gst.hasReverse) {
        if (true) {
            Comparator<Ocurrence> comparator = new Comparator<Ocurrence>() {
                @Override
                public int compare(Ocurrence a, Ocurrence b) {
                    int ret = 0;
                    int aTotalRhsLen = g.rule(a.nonTerminal.getValue()).length();
                    int bTotalRhsLen = g.rule(b.nonTerminal.getValue()).length();
                    assert (!a.nonTerminal.equals(b.nonTerminal) || (aTotalRhsLen == bTotalRhsLen));
                    assert (a.nonTerminal.isNonterminal());
                    assert (b.nonTerminal.isNonterminal());
                    assert (!a.nonTerminal.isUniqueTerminator());
                    assert (!b.nonTerminal.isUniqueTerminator());
                    if (a.nonTerminal.getValue() < b.nonTerminal.getValue()) { 
                        ret = -1;
                    } else if (a.nonTerminal.getValue() > b.nonTerminal.getValue()) {
                        ret = 1;
                    } else if (Ocurrence.adjustedLeftPos(a, aTotalRhsLen, n.length) < Ocurrence.adjustedLeftPos(b,
                            bTotalRhsLen, n.length)) {
                        ret = 1;
                    } else if (Ocurrence.adjustedLeftPos(a, aTotalRhsLen, n.length) > Ocurrence.adjustedLeftPos(b,
                            bTotalRhsLen, n.length)) {
                        ret = -1;
                    } else if (!a.nonTerminal.isComplemented() && b.nonTerminal.isComplemented()) {
                        ret = -1;
                    } else if (a.nonTerminal.isComplemented() && !b.nonTerminal.isComplemented()) {
                        ret = 1; 
                    } else if (!a.nonTerminal.isReversed() && b.nonTerminal.isReversed()) {
                        ret = -1;
                    } else if (a.nonTerminal.isReversed() && !b.nonTerminal.isReversed()) {
                        ret = 1;
                    }
                    return ret;
                }
            };
            n.filteredNonOverlapingMatches = new TreeSet<Ocurrence>(comparator);
        } else {
            //n.filteredNonOverlapingMatches = new TreeSet<Ocurrence>();
        }
        n.filteredNonOverlapingMatches.addAll(n.matches);
        Iterator<Ocurrence> it = n.filteredNonOverlapingMatches.iterator();

        while (it.hasNext()) {
            Ocurrence oc = it.next();
            int totalRhsLen = g.rule(oc.nonTerminal.getValue()).length();
            int adjlpos = Ocurrence.adjustedLeftPos(oc, totalRhsLen, n.length);
            if (debug) {
                System.out.println("oc=" + oc.toString() + " trhslen=" + totalRhsLen + " adj=" + adjlpos);
                
            }
            if (oc.nonTerminal.getValue() == lastRule && adjlpos + n.length > lastLeftPostion) {
                if (debug) {
                    System.out.println("remove");
                    System.out.println("lastLeftPostion="+lastLeftPostion);
                    System.out.println("adjlpos="+adjlpos);
                    System.out.println("adjlpos + n.length="+ (adjlpos + n.length));
                    
                }
                it.remove();
            } else {
                if (debug) {
                    System.out.println("increment");
                    System.out.println("lastLeftPostion="+lastLeftPostion);
                }
                ++count;
                lastRule = oc.nonTerminal.getValue();
                lastLeftPostion = adjlpos;
            }
        }
        if (debug) {
            System.out.println("B>>count=" + count + " n.filteredNonOverlapingMatches.size()="
                    + n.filteredNonOverlapingMatches.size());
            System.out.println("lastLeftPostion="+lastLeftPostion);
        }
        return count;
    }

    protected ONodeInfo createNodeInfo(ONode choosenSubNode) {
        ONodeInfo ni = new ONodeInfo();
        ni.node = choosenSubNode;
        ni.nonOverlaopingOcurrences = false;
        if (null != choosenSubNode) {
            if (null == choosenSubNode.filteredNonOverlapingMatches) {
                ni.ocurrences = choosenSubNode.matches;
            } else {
                ni.ocurrences = choosenSubNode.filteredNonOverlapingMatches;
            }
            ni.nonOverlaopingOcurrences = true;
        }
        if (debug) {
            System.out.println("ni.node=" + ni.node);
            System.out.println("ni.ocurrences=" + ni.ocurrences);
            System.out.println("ni.nonOverlaopingOcurrences=" + ni.nonOverlaopingOcurrences);
            if (null != ni.node) {
                System.out.println("ni.node.getPattern().toString()=" + ni.node.getPattern().toString());
            }
        }
        return ni;
    }

    abstract protected ONodeInfo selectOcurrence(OSeq path, ONode n);

    static public OOSC getOSC(String oscType, OGST gst, Set<OSeq> blacklist, Grammar g) {
        switch (oscType) {
        case "ML":
            return new OOSC_ML(gst, blacklist, g);
        case "MC":
            return new OOSC_MC(gst, blacklist, g);
        case "MO":
            return new OOSC_MO(gst, blacklist, g);
        }
        return null;
    }

    public static void testOcurrences(String rhs, String pattern, String oscType, Integer... Z) {
        testOcurrences(new OSeq(rhs), pattern, oscType, Z);
    }

    public static void testOcurrences(OSeq rhs, String pattern, String oscType, Integer... Z) {
        OGrammar g = new OGrammar(false);
        g.addRule(rhs);
        ONodeInfo ni = (getOSC(oscType, g.getRhsGST(false,false, false), null, g)).selectOcurrence();
        if (ni.ocurrences.size() != (Z.length)) {
            System.err.println("Error in testOcurrences(" + rhs + ",...)");
            System.err.println("\t found " + ni.ocurrences.size() + " ocurrences, expected " + (Z.length));
        } else if (!ni.node.getPattern().toString().equals(pattern)) {
            System.err.println("Error in testOcurrences(" + rhs + ",...)");
            System.err.println("\t found pattern:" + ni.node.getPattern().toString() + " expected:" + pattern);
        } else {
            for (int i = 0; i < Z.length; i++) {
                Ocurrence p = new Ocurrence(ASymbol.nonTerminal((short) 0), Z[i]);
                if (!ni.ocurrences.contains(p)) {
                    System.err.println("Error in testOcurrences(" + rhs + ",...)");
                    System.err.println("\t ocurrence " + p + " not found");
                }
            }
        }
    }

    static protected boolean reducesGrammar(ONode n) {
        /*
         * criterio usado eh a definicao padrao, soma dos rhs se for tam da
         * representacao a funcao e diferente
         */
        int nmatches;
        if (null == n.filteredNonOverlapingMatches) {
            nmatches = n.matches.size();
        } else {
            nmatches = n.filteredNonOverlapingMatches.size();
        }
        if (nmatches < 2)
            return false;
        boolean ret = false;
        ret |= ((2 == n.length) && (3 <= nmatches));
        ret |= (3 <= n.length);
        return ret;
    }


    public static void multiPatternTest(String oscType, boolean complement, boolean reverse, String... sar) {
        OGrammar g = new OGrammar(false);
        for (int i = 0; i < sar.length; ++i) {
            g.addRule(sar[i]);
        }
        OGST gst = g.getRhsGST(complement,reverse, false);
        // gst.DUMP();
        P_OSeqSet localblacklist = new P_OSeqSet(); 
        localblacklist.obj = new TreeSet<OSeq>(new OSeqLengthComparator());
        OOSC oosc = getOSC(oscType, gst, localblacklist.obj, g);
        while (true) {
            ONodeInfo ni = (oosc).selectOcurrence();
            if (null == ni.ocurrences)  return;
            // System.out.println("Pattern=" + ni.node.getPattern().toString());
            // System.out.println("Ocurrences=" +ni.ocurrences.size());
            // System.out.println("Score=" +ni.node.score);
        }

    }

    public static void test(String oscType, String pattern, int count, String sComplement, boolean reverse, String... sar) {
        OGrammar g = new OGrammar(false);
        for (int i = 0; i < sar.length; ++i) {
            g.addRule(sar[i]);
        }
        if (null!=sComplement) {
            if (sComplement.equals("B")) {
                MAGR.complement=new BinaryComplement();
            } else if (sComplement.equals("R")) {
                MAGR.complement=new RNAComplement();
            } else if (sComplement.equals("D")) {
                MAGR.complement=new DNAComplement();
            } else {
                throw new RuntimeException("Invalid Complement Parameter");
            }
        }
        
        OGST gst = g.getRhsGST(sComplement !=null, reverse, false);
        // gst.DUMP();
        ONodeInfo ni = (getOSC(oscType, gst, null, g)).selectOcurrence();
        if (0 == count && null == ni.ocurrences)
            return;
        int found = null==ni.ocurrences?0:ni.ocurrences.size();
        if (count != found) {
            System.out.println("Error count=" + found + " expected:" + count);
            if (null!=ni.ocurrences) for (Ocurrence o: ni.ocurrences) {
                System.out.println("o="+o.toString());
            }
        }
        String foundPattern =  null==ni.ocurrences?null:ni.node.getPattern().toString();
        if (null==foundPattern || !pattern.contentEquals(foundPattern)) {
            System.out.println("Error pattern=" + foundPattern + " expected:" + pattern);
        }
        assert (count == found);
        assert (pattern.contentEquals(foundPattern));
    }

    public static void main(String args[]) {
        multiPatternTest("MC", false, false, "xaxbxcxAxbxcxaxBxcxaxbxC");
        //multiPatternTest("ML", false, "xaxbxcxAxbxcxaxBxcxaxbxC");
        // ML nao preenche score, ok
        //multiPatternTest("MO", false, "xaxbxcxAxbxcxaxBxcxaxbxC");
        // O MO do jeito que est� implementado *N�O* pega os super padr�es
        // Isso est� OK para o IRR porque se diminuir o subpadr�o
        // ent�o o super padr�o desapareceu
        // Pode dar problema no IRCOO/ otimiza��o local?

        OOSC_MC.main(args);
        OOSC_ML.main(args);
        OOSC_MO.main(args);
    }

}
