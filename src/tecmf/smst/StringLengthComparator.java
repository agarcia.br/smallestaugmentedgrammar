package tecmf.smst;

import tecmf.*;

import java.util.*;

//StringLengthComparator.java
import java.util.Comparator;

public class StringLengthComparator implements Comparator<String> {
    @Override
    public int compare(String s1, String s2) {
        if (s1.length() > s2.length()) {
            /* do maior para o menor */
            return -1;
        } else if (s1.length() < s2.length()) {
            return 1;
        } else {
            return s1.compareTo(s2);
        }
    }
}