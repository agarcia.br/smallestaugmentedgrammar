package tecmf.smst;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

import tecmf.utils.Pair;

import java.util.Map;
import java.util.TreeMap;

/**
 * Represents a node of the generalized suffix tree graph
 * 
 * @see GST
 */
public class ONode {

    static long nextid = 0;
    long id;
    public TreeSet<Ocurrence> matches;
    public TreeSet<Ocurrence> filteredNonOverlapingMatches;
    public int score;
    public final Map<ASymbol, OArrow> arrows;
    public ONode suffix;
    public ONode parent;
    public OArrow parentArrow;
    public int length;

    ONode() {
        // arrows = new HashMap<ASymbol,OArrow>();
        arrows = new TreeMap<ASymbol, OArrow>();
        suffix = null;
        matches = new TreeSet<Ocurrence>();
        filteredNonOverlapingMatches = null;
        id = nextid++;
        length = 0;
        parent = null;
        parentArrow = null;
        score = 0;
    }

    public String toString() {
        String s = "[" + this.id + "]";
        s += "[";
        for (Ocurrence p : matches) {
            s += p.toString() + " ";
        }
        s += "]";
        s += "parent:" + (null == parent ? "null" : parent.id) + ",";
        s += "len:" + length + ",";
        s += "edg:";
        for (ASymbol c : arrows.keySet()) {
            s += "{" + c.toString();
            OArrow e = arrows.get(c);
            s += "}->{" + e.label.toString() + "," + e.dest.id + "} ";
        }
        if (null != suffix) {
            s += ", suffix=" + suffix.id;
        }
        return s;
    }

    public OSeq getPattern() {
        OSeq pattern = new OSeq();
        ONode n = this;
        while (null != n.parent) {
            pattern.preppend(n.parentArrow.label);
            n = n.parent;
        }
        return pattern;
    }

    /* return collection of matches */
    Collection<Ocurrence> getMatches() {
        if (arrows.isEmpty()) {
            /* Leaf */
            return matches;
        } else {
            /* Internal Node */
            TreeSet<Ocurrence> ret = new TreeSet<Ocurrence>();
            for (OArrow e : arrows.values()) {
                ret.addAll(e.dest.getMatches());
            }
            return ret;
        }
    }
    /* return collection of matches */
    /*
     * Collection<Ocurrence> getMatches() { if (arrows.isEmpty()) { // Leaf } else {
     * // Internal Node if (0==matches.size()) { for (Arrow e : arrows.values()) {
     * matches.addAll(e.dest.getMatches()); } } } return matches; }
     */

    void addRefL2B(ASymbol nonTerRuleWhereOccurs, int totalRhsLength) {
        int leftEndPosition = totalRhsLength - length;
        addRef(new Ocurrence(nonTerRuleWhereOccurs, leftEndPosition));
    }

    private void addRef(Ocurrence m) {
        if (matches.contains(m)) {
            return;
        }
        matches.add(m);
        // nmatches++;
        // add this reference to all the suffixes as well
        ONode iter = this.suffix;
        while (iter != null) {
            if (iter.matches.contains(m)) {
                break;
            }
            iter.addRef(m);
            iter = iter.suffix;
        }

    }

    void addArrow(ASymbol s, OArrow e) {
        arrows.put(s, e);
    }

    OArrow getArrow(ASymbol s) {
        return arrows.get(s);
    }

    ONode getSuffix() {
        return suffix;
    }

    void setSuffix(ONode suffix) {
        this.suffix = suffix;
    }

}
