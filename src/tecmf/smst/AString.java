package tecmf.smst;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

import tecmf.utils.Persistent;

abstract public class AString extends Persistent {

    private static final long serialVersionUID = -7741509712294312057L;
    protected static boolean debug = false;

    abstract public int length();

    abstract public ASymbol get(int i);

    abstract public ASymbol getFirst();

    abstract public ASymbol getLastNonTerminator();

    abstract public Object clone();

    abstract public AString append(short s);

    abstract public int encodeSize();

    abstract public AString subseq(int beginindex, int endindex);
    
    abstract public void writeToStream(DataOutputStream dos) throws IOException;

}
