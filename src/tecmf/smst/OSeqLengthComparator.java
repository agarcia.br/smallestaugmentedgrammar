package tecmf.smst;

import tecmf.*;

import java.util.*;

//StringLengthComparator.java
import java.util.Comparator;

public class OSeqLengthComparator implements Comparator<OSeq> {
    @Override
    public int compare(OSeq s1, OSeq s2) {
        if (s1.length() > s2.length()) {
            /* do maior para o menor */
            return -1;
        } else if (s1.length() < s2.length()) {
            return 1;
        } else {
            return s1.toString().compareTo(s2.toString());
        }
    }
}