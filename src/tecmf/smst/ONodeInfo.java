package tecmf.smst;

import java.util.*;

import tecmf.utils.Pair;

public class ONodeInfo {
    public boolean nonOverlaopingOcurrences;
    public ONode node;
    // public SSeq substring; n�o precisa porque node tem length
    public TreeSet<Ocurrence> ocurrences; // Set<<string index,beginposition>>

}
