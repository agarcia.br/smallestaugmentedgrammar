package tecmf.smst;

import java.io.*;
import java.util.*;

import tecmf.MAGR;
import tecmf.ircoo.YieldIter;
import tecmf.smst.*;
import tecmf.utils.Persistent;

abstract public class Grammar extends Persistent {

    private static final long serialVersionUID = 998466760023662298L;
    public boolean recursiveSemantics = false;
    public static final short IRR = 256;
    public short ruleQtt = 0;
    public Vector<AString> rules = new Vector<AString>();

    abstract public AString rule(int i);

    abstract public AString generateString();

    public Grammar() {
        super();
    }

    public void saveBinary() {
        String outputName = MAGR.inputFile + MAGR.parameters + "_binary_grammar.dat";
        // 0x00 will be scaped as //0x00 0x00
        // Nonterminals will be scaped as //0x00 0x01 <short>
        // Rversed Nonterminals will be scaped as //0x00 0x02 <short>
        // so breaks can be coded as //0x00 0x0A
        try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outputName)))) {
            for (int i = 0; i < ruleQtt; ++i) {
                if (i != 0) {
                    dos.writeByte(0x00);
                    dos.writeByte(0x0A);
                }
                rule(i).writeToStream(dos);
            }
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ruleQtt; ++i) {
            sb.append("r" + i + "->" + rule(i).toString() + "\n");
        }
        return sb.toString();
    }

    abstract public void addRule(String s);

    public int size() {
        int si = 0;
        for (int i = 0; i < rules.size(); ++i) {
            AString r = rules.get(i);
            assert (null != r);
            // By definition of Charikar et al. (2005), "The Smallest Grammar Problem"
            // Size should include only rhs, without the unique terminator
            si += r.length() - 1;
        }
        return si;
    }

    public int encodeSize() {
        int si = 0;
        for (int i = 0; i < rules.size(); ++i) {
            AString r = rules.get(i);
            assert (null != r);
            si += r.encodeSize();
        }
        return si;
    }

}
