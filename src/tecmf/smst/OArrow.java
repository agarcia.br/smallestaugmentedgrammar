package tecmf.smst;

class OArrow {
    public OSeq label;
    public ONode dest;

    public OArrow(OSeq label, ONode dest) {
        this.label = label;
        this.dest = dest;
    }

}
