package tecmf.smst;

//StringLengthComparator.java
import java.util.Comparator;

public class ONodeCompactComparator implements Comparator<ONode> {

    boolean breakTieFavorsLonger;
    
    ONodeCompactComparator(boolean breakTieFavorsLonger) {
        this.breakTieFavorsLonger = breakTieFavorsLonger; 
    }
    
    @Override
    public int compare(ONode x, ONode y) {
        int ret = y.score - x.score;
        if (0== ret && breakTieFavorsLonger) {
            //ret = y.length - x.length;
            ret = x.length - y.length;
        }
        return ret;
    }
}