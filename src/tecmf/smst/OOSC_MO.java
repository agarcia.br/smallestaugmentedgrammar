package tecmf.smst;

import java.util.*;

public class OOSC_MO extends OOSC {
    /* Ocurrence Selection Criteria: or (o)ccurrences */

    public OOSC_MO(OGST gst, Set<OSeq> blacklist, Grammar g) {
        super(gst, blacklist, g);
        fillMatchesAndScore(gst.root);
    }

    protected int getScore(ONode n) {
        int o = n.filteredNonOverlapingMatches.size();
        n.score = reducesGrammar(n) ? o : 0;
        return n.score;
    }

    int maxcount2;
    ONode maxSubNode2;
    int maxcount3;
    ONode maxSubNode3;

    private void updateMaxSubNode(ONode subNode) {
        if (2 <= subNode.length) {
            // int count = fillNonOverlappingMatches(subNode);
            int count = 0;
            if (null == subNode.filteredNonOverlapingMatches) {
                if (debug)
                    System.out.println("subNode.id=" + subNode.id + " filtered=null");
            } else {
                count = subNode.filteredNonOverlapingMatches.size();
            }
            // int count = subNode.matches.size();
            if (blacklisted(subNode))
                count = 0;
            if (maxcount2 < count) {
                maxcount2 = count;
                maxSubNode2 = subNode;
            }
            if (3 <= subNode.length) {
                if (maxcount3 < count) {
                    maxcount3 = count;
                    maxSubNode3 = subNode;
                }
            }
        }
    }

    protected ONodeInfo selectOcurrence(OSeq path, ONode n) {
        TreeSet<Ocurrence> localOcurrences = new TreeSet<Ocurrence>();
        maxcount2 = 1; // only nodes with > 1 ocurrences will be detected
        maxSubNode2 = null;
        maxcount3 = 1; // only nodes with > 1 ocurrences will be detected
        maxSubNode3 = null;
        for (OArrow e : n.arrows.values()) {
            ONode subNode = e.dest;
            if (null == subNode)
                continue;
            assert (0 < subNode.length);
            if (1 == subNode.length) {
                // in this case look at second generation for patterns of length 2 or 3
                for (OArrow e2 : subNode.arrows.values()) {
                    updateMaxSubNode(e2.dest);
                }
            } else {
                updateMaxSubNode(subNode);
            }
        }
        if (null == maxSubNode2 && null == maxSubNode3) {
            /*
             * aqui pode haver problema pode ser que os filhos de um subNode2 tenham ao
             * menos 2 ocorrencias como seu tamanho � ao menos 3, podem servir aparentemente
             * seria f�cil programar, mas n�o foi encontrado contra-exemplo talvez porque os
             * matches s�o sempre maximais na GST
             */
        }
        ONode choosenSubNode = null;
        if (null == maxSubNode2 || maxcount2 <= maxcount3) {
            choosenSubNode = maxSubNode3;
        } else {
            assert (null != maxSubNode2);
            assert (maxcount2 > maxcount3);
            if (2 < maxcount2)
                choosenSubNode = maxSubNode2;
        }

        ONodeInfo ni = createNodeInfo(choosenSubNode);
        blacklist(ni.node);
        return ni;
    }

    public static void main(String args[]) {
        test("MO", "xax", 7, null, false, "xaxbxcxAxbxcxaxBxcxaxbxCxaxcxbxDxbxaxcxExcxbxaxFxaxGxbxHxcx");
        test("MO", "ab", 3, null, false, "ababab");
        test("MO", "abcd", 3, null, false, "abcdabcdabcd");
        test("MO", "ab", 3, null, false, "abab", "ab");
        test("MO", "abc", 2, null, true, "abcdef", "XcbaY");
        test("MO", "ab", 4, null, true, "abcdef", "XcbaYbaZba");
        test("MO", "ab", 3, null, true, "abcdef", "XcbaYba");
        test("MO", "ab", 3, null, true, "ab", "baba");
        test("MO", "xax", 2, null, true, "xaxbxAxbxaxB");
        test("MO", "bc", 4, null, false, "bcXbcYbcZbc");
        test("MO", "bcc", 2, "B", false, "bccZcbb");
        test("MO", "bb", 4, "B", false, "bccZcbbRbbcYccb");
        test("MO", "bb", 4, null, false, "bccZcbbRbbcYccb","cbb[bccSccbXbbc");
        test("MO", "agc", 2, "D", false, "atcggtagc");
        test("MO", "bb", 4, "B", true, "bccZcbbRbbcYccb");
        test("MO", "xy", 5, null, true, "xyxyxyxyxyabcabcabcabc");
        
        System.out.println("end tests");

    }
}
