package tecmf.smst;

import tecmf.*;
import tecmf.utils.Pair;

import java.util.*;

public class OGST {

    private boolean debug = false;

    Set<ASymbol> uniqueTerminators;
    public boolean hasReverse = false;
    public boolean hasComplement = false;

    public OGST() {
        uniqueTerminators = new HashSet<ASymbol>();
    }

    /**
     * The root of the suffix tree
     */
    public final ONode root = new ONode();
    /**
     * The last leaf that was added during the update operation
     */
    private ONode activeLeaf = root;

    public void traverse() {
        // System.out.println(""+root.toString());
        traverse("", root);
    }

    TreeSet<ONode> visited = null;

    public void traverse(String prefix, ONode n) {
        System.out.println("" + prefix + n.toString());
        for (OArrow e : n.arrows.values()) {
            ONode n1 = e.dest;
            if (null != n1)
                traverse(prefix + "\t", n1);
        }
    }

    public void put(String s) throws IllegalStateException {
        put(new OSeq(s));
    }

    public void put(OSeq key) throws IllegalStateException {
        int keylen = key.length();
        ASymbol terminator = key.get((key.length() - 1));
        assert (terminator.isUniqueTerminator());
        if (uniqueTerminators.contains(terminator)) {
            throw new IllegalStateException("The string must be uniquely terminated");
        }
        uniqueTerminators.add(terminator);
        if (terminator.isReversed()) {
            hasReverse = true;
        }
        if (terminator.isComplemented()) {
            hasComplement = true;
        }

        if (debug)
            System.out.println("key.length()=" + key.length());
        if (debug)
            System.out.println("key=" + key);
//System.err.println("key="+key);

        // reset activeLeaf
        activeLeaf = root;

        OSeq remainder = key;
        ONode activeNode = root;
        if (debug)
            System.out.println("remainder.length()=" + remainder.length());

        // proceed with tree construction (closely related to procedure in Ukkonen's
        // paper)
        OSeq text = new OSeq();
        // iterate over the string, one char at a time
        for (int i = 0; i < remainder.length(); ++i) {
            // line 6
            if (debug)
                System.out.println("remainder.seq[" + i + "]=" + remainder.seq[i]);
            text.append(remainder.seq[i]);
            // if (debug) System.out.println("text="+text);
            // line 7: update the tree with the new transitions due to this new char

            Pair<ONode, OSeq> active = update(activeNode, text, remainder.subseq(i), keylen, terminator);
            // if (debug) System.out.println("active="+active);
            // line 8: make sure the active pair is canonical
            active = canonize(active.first, active.second);
            // if (debug) System.out.println("canonized active="+active);
            activeNode = active.first;
            text = active.second;
            if (debug)
                DUMP();
        }

        // add leaf suffix link, is necessary
        if (null == activeLeaf.getSuffix() && activeLeaf != root && activeLeaf != activeNode) {
            activeLeaf.setSuffix(activeNode);
        }

    }

    /**
     * Tests whether the string stringPart + t is contained in the subtree that has
     * inputs as root. If that's not the case, and there exists a path of edges e1,
     * e2, ... such that e1.label + e2.label + ... + $end = stringPart and there is
     * an edge g such that g.label = stringPart + rest
     * 
     * Then g will be split in two different edges, one having $end as label, and
     * the other one having rest as label.
     *
     * @param inputs     the starting node
     * @param stringPart the string to search
     * @param t          the following character
     * @param remainder  the remainder of the string to add to the index
     * @param value      the value to add to the index
     * @return a pair containing true/false depending on whether (stringPart + t) is
     *         contained in the subtree starting in inputs the last node that can be
     *         reached by following the path denoted by stringPart starting from
     *         inputs
     * 
     */
    private Pair<Boolean, ONode> testAndSplit(final ONode inputs, final OSeq stringPart, final ASymbol t,
            final OSeq remainder, final int lenString, final ASymbol terminator) {
        // descend the tree as far as possible
        Pair<ONode, OSeq> ret = canonize(inputs, stringPart);
        // if (debug) System.out.println("in split canonize ret="+ret);
        ONode s = ret.first;
        OSeq str = ret.second;

        if (0 < str.seq.length) {
            OArrow g = s.getArrow(str.seq[0]);
            // if (debug) System.out.println("in split Arrow g="+g);

            OSeq label = g.label;
            // if (debug) System.out.println("in split Arrow g.label="+g.label);
            if (label.length() > str.length() && label.seq[str.length()].equals(t)) {
                // stringPart.substring(?) + t esta incluido em label
                return new Pair<Boolean, ONode>(true, s);
            } else {
                // need to split the edge
                OSeq newlabel = label.subseq(str.length());
                assert (label.startsWith(str));
                // s -> (g)
                ONode r = new ONode();
                OArrow newedge = new OArrow(str, r);
                r.length = s.length + str.length();
                r.parent = s;
                r.parentArrow = newedge;
                g.label = newlabel;
                g.dest.parent = r;
                r.addArrow(newlabel.seq[0], g);
                s.addArrow(str.seq[0], newedge);
                // s ->(newedge) r -> (g)
                return new Pair<Boolean, ONode>(false, r);
            }

        } else {
            // 0 == str.seq.length; chega exatamente em um n�
            OArrow e = s.getArrow(t);
            // if (debug) System.out.println("in split t="+t+"<");
            // if (debug) System.out.println("in split OArrow e="+e);
            if (null == e) {
                // if there is no t-transtion from s
                return new Pair<Boolean, ONode>(false, s);
            } else {
                if (remainder.equals(e.label)) {
                    // update payload of destination node
                    ASymbol nonTerRuleWhereOccurs = ASymbol.nonTerminalFromTerminator(terminator);
                    e.dest.addRefL2B(nonTerRuleWhereOccurs, lenString);
                    return new Pair<Boolean, ONode>(true, s);
                } else if (remainder.startsWith(e.label)) {
                    return new Pair<Boolean, ONode>(true, s);
                } else if (e.label.startsWith(remainder)) {
                    // need to split as above
                    // s->(e)
                    ONode newNode = new ONode();
                    newNode.length = s.length + remainder.length();
                    newNode.parent = s;
                    ASymbol nonTerminal = ASymbol.nonTerminalFromTerminator(terminator);
                    newNode.addRefL2B(nonTerminal, lenString);

                    OArrow newEdge = new OArrow(remainder, newNode);
                    newNode.parentArrow = newEdge;
                    e.label = e.label.subseq(remainder.length());
                    e.dest.parent = newNode;
                    newNode.addArrow(e.label.seq[0], e);
                    s.addArrow(t, newEdge);
                    // s->(newEdge)newNode->(e)

                    return new Pair<Boolean, ONode>(false, s);
                } else {
                    // they are different words. No prefix. but they may still share some common
                    // substr
                    return new Pair<Boolean, ONode>(true, s);
                }
            }
        }

    }

    /**
     * Return a (node, restOfPath) such that delta(s,beginOfPath) = node and
     * beginOfPath+restOfPath = path
     */
    private Pair<ONode, OSeq> canonize(final ONode s, final OSeq path) {
        Pair<ONode, OSeq> ret = null;
        if (0 == path.length()) {
            ret = new Pair<ONode, OSeq>(s, path);
        } else {
            ONode currentNode = s;
            OSeq restOfPath = path.clone();
            OArrow aLink = s.getArrow(path.seq[0]);
            // descend the tree as long as a proper label is found
            while (aLink != null && restOfPath.startsWith(aLink.label)) {
                restOfPath = restOfPath.subseq(aLink.label.length());
                currentNode = aLink.dest;
                if (0 < restOfPath.seq.length) {
                    aLink = currentNode.getArrow(restOfPath.seq[0]);
                }
            }
            ret = new Pair<ONode, OSeq>(currentNode, restOfPath);
        }
        if (false) {
            System.out.println("canonize(");
            System.out.println("\ts=" + s);
            System.out.println("\tpath=" + path);
            System.out.println(")=" + ret);
        }
        return ret;
    }

    /**
     * Updates the tree starting from inputNode and by adding stringPart.
     * 
     * Returns a reference (Node, String) pair for the string that has been added so
     * far. This means: - the Node will be the Node that can be reached by the
     * longest path string (S1) that can be obtained by concatenating consecutive
     * edges in the tree and that is a substring of the string added so far to the
     * tree. - the String will be the remainder that must be added to S1 to get the
     * string added so far.
     * 
     * @param inputNode the node to start from
     * @param pSeqPart  the string to add to the tree
     * @param rest      the rest of the string
     * @param strNumber the value to add to the index
     */
    private Pair<ONode, OSeq> update(final ONode inputNode, final OSeq pSeqPart, final OSeq rest, final int keyLen,
            final ASymbol terminator) {

        if (debug) {
            System.out.println("update(");
            System.out.println("\t inputNode=" + inputNode);
            System.out.println("\t pSeqPart=" + pSeqPart);
            System.out.println("\t rest=" + rest);
            System.out.println("\t lenString=" + keyLen);
            System.out.println("\t terminator=" + terminator);
            System.out.println(")=...");
        }

        ONode activeNode = inputNode;
        OSeq seqPart = pSeqPart.clone();
        ASymbol newChar = pSeqPart.seq[pSeqPart.length() - 1];

        // line 1
        ONode oldroot = root;

        // line 1b
        Pair<Boolean, ONode> ret = testAndSplit(activeNode, seqPart.subseq(0, seqPart.length() - 1), newChar, rest,
                keyLen, terminator);
        // if (debug) System.out.println("split ret="+ret);
        ONode r = ret.second;
        boolean endpoint = ret.first;

        ONode leaf = null;
        // line 2
        while (!endpoint) {
            // line 3
            OArrow tempEdge = r.getArrow(newChar);
            // if (debug) System.out.println("tempEdge="+tempEdge);
            if (null != tempEdge) {
                // such a node is already present. This is one of the main differences from
                // Ukkonen's case:
                // the tree can contain deeper nodes at this stage because different strings
                // were added by previous iterations.
                leaf = tempEdge.dest;
            } else {
                // must build a new leaf
                // if (1!=terminator.getValue()) {
                leaf = new ONode();
                leaf.parent = r;
                leaf.length = r.length + rest.length();
                ASymbol nonTerminal = ASymbol.nonTerminalFromTerminator(terminator);
                leaf.addRefL2B(nonTerminal, keyLen);
                OArrow newedge = new OArrow(rest, leaf);
                leaf.parentArrow = newedge;
                r.addArrow(newChar, newedge);
                // }
            }

            // update suffix link for newly created leaf
            if (activeLeaf != root) {
//if (null!=leaf)            	
                activeLeaf.setSuffix(leaf);
            }
            activeLeaf = leaf;
            // if (debug) System.out.println("activeLeaf="+activeLeaf);

            // line 4
            if (oldroot != root) {
                oldroot.setSuffix(r);
            }

            // line 5
            oldroot = r;

            // line 6
            if (null == activeNode.getSuffix()) { // root node
                assert (root == activeNode);
                // this is a special case to handle what is referred to as node _|_ on the paper
                seqPart = seqPart.subseq(1);
            } else {
                Pair<ONode, OSeq> canret = canonize(activeNode.getSuffix(), safeCutLastChar(seqPart));
                // if (debug) System.out.println("canret="+canret);
                activeNode = canret.first;
                // use intern to ensure that tempstr is a reference from the string pool
                OSeq tempseq2 = canret.second.clone();
                tempseq2.append(seqPart.seq[seqPart.length() - 1]);
                seqPart = tempseq2;
            }

            // line 7
            ret = testAndSplit(activeNode, safeCutLastChar(seqPart), newChar, rest, keyLen, terminator);
            r = ret.second;
            endpoint = ret.first;

        }

        // line 8
        if (oldroot != root) {
            oldroot.setSuffix(r);
        }
        oldroot = root;

        Pair<ONode, OSeq> ret2 = new Pair<ONode, OSeq>(activeNode, seqPart);
        if (debug) {
            System.out.println("update()...=" + ret2);
        }

        return ret2;
    }

    ONode getRoot() {
        return root;
    }

    private OSeq safeCutLastChar(OSeq seq) {
        if (seq.length() == 0) {
            return new OSeq();
        }
        return seq.subseq(0, seq.length() - 1);
    }

    public void DUMP() {
        OGSTVisitor v = new OGSTVisitor(OGSTVisitor.DEPTH_FIRST_POST_ORDER) {
            public void visit(ONode n) {
                System.out.println(n.toString());
            }

            public void visit(ONode n, int level) {
                System.out.print("\t".repeat(level));
                visit(n);
            }
        };
        System.out.println(">>>");
        v.traverse(root, 0);
        System.out.println("<<<");
    }

    public static void main(String args[]) {
        OGrammar g = new OGrammar(false);
        g.addRule("abc");
        g.addRule("ab");
        OGST gst = g.getRhsGST(false, false, false);
        gst.DUMP();
    }

}
