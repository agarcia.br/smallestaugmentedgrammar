package tecmf.smst;

import java.util.*;

public class Ocurrence implements Comparable {
    public ASymbol nonTerminal; // Rule or implicit rule where the pattern is found
    public int leftEndPosition;

    public Ocurrence(ASymbol index, int beginpos) {
        this.nonTerminal = index;
        this.leftEndPosition = beginpos;
    }

    @Override
    public String toString() {
        return "" + nonTerminal + ":" + leftEndPosition;
    }

    @Override
    public boolean equals(Object o) {
        boolean ret = false;
        if (o instanceof Ocurrence) {
            Ocurrence oc = (Ocurrence) o;
            ret = toString().equals(oc.toString());
        }
        // System.err.println("equals="+ret);
        return ret;
    }

    @Override
    public int hashCode() {
        // return Integer.hashCode(2719*nonTerminal.hashCode()+leftEndPosition);
        return toString().hashCode();
    }

    /*
     * 
     * @Override public int compareTo(Object o) { int ret =0; if (! (o instanceof
     * Ocurrence )) { return ret; } Ocurrence oc = (Ocurrence)o; assert
     * (!nonTerminal.equals(oc.nonTerminal)||(totalRhsLen==oc.totalRhsLen));
     * assert(nonTerminal.isNonterminal()); assert(oc.nonTerminal.isNonterminal());
     * assert(!nonTerminal.isUniqueTerminator());
     * assert(!oc.nonTerminal.isUniqueTerminator());
     * assert(0==nonTerminal.getShiftValue());
     * assert(0==oc.nonTerminal.getShiftValue()); if (nonTerminal.getValue() <
     * oc.nonTerminal.getValue()) { ret = -1; } else if (nonTerminal.getValue() >
     * oc.nonTerminal.getValue()) { ret = 1; } else if (adjustedLeftPos() <
     * oc.adjustedLeftPos()) { ret = 1; } else if (adjustedLeftPos() >
     * oc.adjustedLeftPos()) { ret = -1; } else if (!nonTerminal.isReversed() &&
     * oc.nonTerminal.isReversed()) { ret = -1; } else if (nonTerminal.isReversed()
     * && !oc.nonTerminal.isReversed()) { ret = 1; } return ret; }
     */

    @Override
    public int compareTo(Object o) {
        // The order of ocurrences to work with substitution must be:
        // 1. grouped by ASymbol
        // 2. decreasing order by Integer (position)
        int ret = 0;
        if (o instanceof Ocurrence) {
            assert (nonTerminal.isNonterminal());
            assert (!nonTerminal.isUniqueTerminator());
            Ocurrence oc = (Ocurrence) o;
            ret = nonTerminal.compareTo(oc.nonTerminal);
            if (0 == ret) {
                if (leftEndPosition < oc.leftEndPosition) {
                    ret = 1;
                } else if (leftEndPosition > oc.leftEndPosition) {
                    ret = -1;
                }
            }
        }
        // System.err.println("compareTo="+ret);
        return ret;
    }

    static public int adjustedLeftPos(Ocurrence c, int totalRhsLen, int patternlen) {
        assert (c.nonTerminal.isNonterminal());
        if (!c.nonTerminal.isReversed())
            return c.leftEndPosition;
        int rightEndPosition = c.leftEndPosition + patternlen - 1;
        if (rightEndPosition == totalRhsLen - 1)
            return rightEndPosition;
        int rhsLenNoTerminator = totalRhsLen - 1;
        int adjustedLeft = rhsLenNoTerminator - 1 - rightEndPosition;
        return adjustedLeft;
    }

    private static void testMatchesIteratorOrder() {
        ASymbol nt[] = new ASymbol[3];
        ASymbol ntr[] = new ASymbol[nt.length];
        for (short i = 0; i < nt.length; ++i) {
            nt[i] = ASymbol.nonTerminal(i);
            ntr[i] = ASymbol.reverseNonTerminalFromNonterminal(nt[i]);

        }
        int positions[] = { 0, 3, 8 };
        Ocurrence o[] = new Ocurrence[nt.length * 2 * positions.length];
        int k = 0;
        ONode dummy = new ONode();
        dummy.length = 3;
        for (short j = 0; j < positions.length; ++j) {
            for (short i = 0; i < nt.length; ++i) {
                o[k++] = new Ocurrence(nt[i], positions[j]);
            }
            for (short i = 0; i < nt.length; ++i) {
                o[k++] = new Ocurrence(ntr[i], positions[j]);
            }
        }
        /*
         * Para calcular se h� overlapping termos que ter o tamanho associado a cada
         * regra... para isso temos que ter a gram�tica ou guardar no OGST, a grammatica
         * pode ser passada para o construtor da OOSC, por hora ignorar o len do padrao
         * e o len total, verificar agrupamento
         */

        printMatches(o, 1);
        printMatches(o, 5);
        printMatches(o, 7);
        printMatches(o, 11);
        printMatches(o, 13);
        printMatches(o, 17);
    }

    private static void printMatches(Ocurrence o[], int factor) {
        TreeSet<Ocurrence> matches = new TreeSet<Ocurrence>(); // o.length = 2*3*3 = 18
        int j = 0;
        for (int i = 0; i < o.length; ++i) {
            matches.add(o[j]);
            j = ((j + factor) % o.length);
        }
        Iterator<Ocurrence> it = matches.iterator();
        System.out.println(">>> factor=" + factor);
        while (it.hasNext()) {
            Ocurrence oc = it.next();
            System.out.println("Oc=" + oc);
        }
        System.out.println("<<<");
    }

    public static void main(String args[]) {

        testMatchesIteratorOrder();
        System.out.println("end tests");
    }

}
