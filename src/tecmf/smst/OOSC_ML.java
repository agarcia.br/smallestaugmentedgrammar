package tecmf.smst;

import tecmf.*;

import java.util.*;

public class OOSC_ML extends OOSC {
    /* Ocurrence Selection Criteria :(l)ength */

    public OOSC_ML(OGST gst, Set<OSeq> blacklist, Grammar g) {
        super(gst, blacklist, g);
        queueNodes(gst.root);
    }

    protected int getScore(ONode n) {
        // ni.ocurrences.size()
        int alen = n.length;
        n.score = reducesGrammar(n) ? alen : 0;
        return n.score;
    }

    private Comparator<ONode> comparator;
    private PriorityQueue<ONode> queue;

    protected void queueNodes(ONode root) {
        comparator = new ONodeLengthComparator();
        queue = new PriorityQueue<ONode>(100, comparator);
        OGSTVisitor v = new OGSTVisitor(OGSTVisitor.DEPTH_FIRST_POST_ORDER) {
            public void visit(ONode n) {
                if (!n.arrows.isEmpty()) {
                    // Internal Nodes Only
                    if (2 > n.length)
                        return; // can not be used
                    queue.add(n);
                }
            }
        };
        v.traverse(root);
    }

    protected ONodeInfo selectOcurrence(OSeq path, ONode n) {

        ONode choosenSubNode = null;
        while (queue.size() != 0) {
            ONode subNode = queue.remove();
            // perform fillMatches as demanded
            for (OArrow e : subNode.arrows.values()) {
                /* By induction hypothesis, dest have bigger length, so are filles */

                // O assert abaixo pode n�o ser verdadeiro devido ao dummy no reverso
                // assert(e.dest.matches.size()>0);
                subNode.matches.addAll(e.dest.matches);
            }
            if ("bbc".contentEquals(subNode.getPattern().toString())) {
                debug = false;
            } else {
                debug = false;
            }
            
            int count = fillNonOverlappingMatches(subNode);
            if (blacklisted(subNode))
                count = 0;
            if (2 < count || (2 == count && subNode.length > 2)) {
                choosenSubNode = subNode;
                break;
            }
        }
        ONodeInfo ni = createNodeInfo(choosenSubNode);
        blacklist(ni.node);
        return ni;
    }

    public static void main(String args[]) {
         
        test("ML", "xaxbx", 2, null, false, "xaxbxcxAxbxcxaxBxcxaxbxCxaxcxbxDxbxaxcxExcxbxaxFxaxGxbxHxcx");
        test("ML", "CDEFGHIJK", 2, null, false, "xaxaxBxaxbCDEFGHIJKxaxCDEFGHIJK");
        test("ML", "abc", 2, null, true, "abcdef", "XcbaY");
        test("ML", "abcd", 2, null, true, "abcdef", "XcbaYdcbaZba");
        test("ML", "xbxax", 2, null, true, "xaxbxAxbxaxB");
        test("ML", "xxax", 2, null, true, "xaxxxxax"); 
        test("ML", "", 0, null, true, "xaxxxxBx");// ok, elimina pelo overlap
        
        test("ML", "bcc", 2, "B", false, "bccZcbb");
        test("ML", "bbc", 2, "B", false, "bccZcbbRbbcYccb");
        test("ML", "bbc", 2, null, false, "bccZcbbRbbcYccb","cbb[bccSccbXbbc");
        test("ML", "atcg", 2, "D", false, "atcggtagc");
        
        test("ML", "ccb", 4, "B", true, "bccZcbbRbbcYccb");

        System.out.println("end tests");

    }

}
