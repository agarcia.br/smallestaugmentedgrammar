package tecmf;

import tecmf.smst.*;

abstract public class Alg {

    abstract public Grammar process(byte buffer[], boolean verbose);
    public boolean debug = false;
    public boolean aReverse;
    public boolean aComplement;

    public Alg() {
        this.aReverse = MAGR.aReverse;
        this.aComplement = MAGR.complement != null;
    }
    
    protected void printMemoryUse() {
        System.out.println("totalMem(M)="+Runtime.getRuntime().totalMemory()/(1024*1024));
        System.out.println("freeMem(M)="+Runtime.getRuntime().freeMemory()/(1024*1024));
        System.out.println("usedMem(M)="+(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory())/(1024*1024));
    }

    protected void endLoop(int a) {
        if (MAGR.verbose && (0 == a % 500)) {
            if (this.debug) {
                System.out.println("iterations=" + a );
            }
            if (MAGR.veryVerbose) printMemoryUse();
        }
        
    }

}
