package tecmf.gene;

import tecmf.smst.AString;
import tecmf.smst.ASymbol;

public class RNAComplement extends Complement {

    @Override
    public char comp(char a) {
        char r=a;
        switch (a) {
            case 'a': r='u'; break;
            case 'A': r='U'; break;
            case 'u': r='a'; break;
            case 'U': r='A'; break;
            case 'g': r='c'; break;
            case 'G': r='C'; break;
            case 'c': r='g'; break;
            case 'C': r='G'; break;
        }
        return r;
    }


}
