package tecmf.gene;

import tecmf.smst.AString;
import tecmf.smst.ASymbol;

public class DNAComplement extends Complement {

    @Override
    public char comp(char a) {
        char r=a;
        switch (a) {
            case 'a': r='t'; break;
            case 'A': r='T'; break;
            case 't': r='a'; break;
            case 'T': r='A'; break;
            case 'g': r='c'; break;
            case 'G': r='C'; break;
            case 'c': r='g'; break;
            case 'C': r='G'; break;
        }
        return r;
    }


}
