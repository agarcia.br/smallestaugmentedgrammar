package tecmf.gene;
import tecmf.smst.*;

abstract public class Complement {

    abstract public char comp (char a);


    public ASymbol comp (ASymbol a) {
        ASymbol ret;
        if (a.isNonterminal()||a.isUniqueTerminator()) {
            ret = a.clone();
            ret.setComplemented(!ret.isComplemented());
        } else {
            assert(a.isTerminal());
            short v = a.getValue();
            assert(0<=v);
            assert(v<=255);
            ret = new ASymbol((short)comp((char)v));
        }
        return ret;
    }

    public AString comp(AString a) {
        OSeq ret = (OSeq) a.clone();
        for (int i=0; i<ret.seq.length; ++i) {
            ret.seq[i] = comp(ret.seq[i]);
        }
        return ret;
    }

    public String comp (String a) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<a.length(); ++i) {
            char c = a.charAt(i);
            sb.append(comp(c));
        }
        return sb.toString();
    }

    static public void test(String s1, String s2, Complement c) {
        String r  =  c.comp(s1);
        if (!s2.equals(r)) {
            System.err.println("Error expected "+s2+" obtained "+r);
        }
        AString a1 = new OSeq(s1);
        AString a2 = new OSeq(s2);
        if (!a1.equals(a1)) {
            System.err.println("Error expected "+a1.toString()+" obtained "+a2.toString());
        }
    }

    static public void main(String args[]) {
        test("aAgGtcTC","tTcCagAG", new DNAComplement());
        test("aAgGucUC","uUcCagAG", new RNAComplement());
        test("@ABCbcZ","A@CBcb[", new BinaryComplement());
        System.out.println("Fim testes");
    }
}
