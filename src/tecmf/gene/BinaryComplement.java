package tecmf.gene;

import tecmf.smst.AString;
import tecmf.smst.ASymbol;

public class BinaryComplement extends Complement {

    /* complements last bit */
    @Override
    public char comp(char a) {
        assert(a>=0);
        assert(a<=256);
        boolean even = (0==a%2);
        char ret =  (char) (even?a+1:a-1);
        assert(ret>=0);
        assert(ret<=256);
        return ret;
    }


}
