package tecmf;

import java.io.*;
import java.util.Set;
import java.util.TreeSet;

import tecmf.smst.OGST;
import tecmf.ircoo.IRCOO_Minimizer;
import tecmf.smst.Grammar;
import tecmf.smst.OGrammar;
import tecmf.smst.ONodeInfo;
import tecmf.smst.OOSC;
import tecmf.smst.OSeq;
import tecmf.smst.OSeqLengthComparator;
import tecmf.smst.StringLengthComparator;
import tecmf.utils.P_OSeqSet;
import tecmf.utils.Persistent;

public class Alg_CountRepetitions extends Alg {

    public String oscType = null;

    P_OSeqSet blacklist = null;

    public Alg_CountRepetitions(String oscType, P_OSeqSet blacklist) {
        super();
        this.oscType = oscType;
        this.debug=false;
        if (null == blacklist) {
            this.blacklist = new P_OSeqSet();
        } else {
            this.blacklist = blacklist;
        }
    }

    public Grammar process(byte buffer[], boolean verbose) {
        OGrammar g;
        g = new OGrammar(true);
        g.addRule(new OSeq(buffer));
        blacklist.obj = new TreeSet<OSeq>(new OSeqLengthComparator());
        int patterns = 0;
        int repetitions = 0;
        int norepetitions = 0;
        OGST gst = null;
        OOSC oosc = null;
        ONodeInfo ni = null;
        gst = g.getYieldGST(aReverse);
        oosc = OOSC.getOSC(oscType, gst, blacklist.obj, g);
        while (true) {
            ni = oosc.selectOcurrence();
            if (!ni.nonOverlaopingOcurrences)
                break;
            patterns++;
            repetitions+=ni.node.matches.size();
            norepetitions+=ni.node.filteredNonOverlapingMatches.size();
        }
        System.out.println("patterns=" + patterns);
        System.out.println("repetitions=" + repetitions);
        System.out.println("norepetitions=" + norepetitions);
        return g;
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
    }

}
