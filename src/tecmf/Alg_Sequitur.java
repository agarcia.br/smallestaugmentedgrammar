package tecmf;

import java.io.*;
import java.util.*;
import tecmf.smst.*;

public class Alg_Sequitur extends Alg {
    
    /* Simplified (slower) version of Sequitur */
    /* Instead of using linked lists and book keeping 
     * a pointer to the first objetc in a pair
     * This version simply keeps the rule number where the pair
     * have been seen before
     */

    static public boolean debugRenumber = false;

	OGrammar g;
	HashMap<OSeq,Short> pointers = new HashMap<OSeq,Short>();
    int   currentFinalPos;
	
	public Alg_Sequitur() {
	    super();
		this.debug=false;
	}


	private void LOG_POINTER(String s, OSeq p) {
        assert(2==p.length());
        System.out.println(s+"**getPointer("+p+")="+getPointer((p)));
	    
	}
    private void DUMP_POINTERS() {
        Set<OSeq> s = (Set<OSeq> ) pointers.keySet();
        System.out.println("************************************");
        for (OSeq p:s) {
            assert(2==p.length());
            System.out.println("**getPointer(("+p+")="+getPointer((p)));
        }
        System.out.println("************************************");
    }


    private OSeq chk(OSeq k) {
        assert (2==k.length()||3==k.length());
        if ((2==k.length()||k.seq[2].isUniqueTerminator())) {
            
        } else {
            System.out.println("k="+k.toString());
        }
        assert (2==k.length()||k.seq[2].isUniqueTerminator());
        return (2==k.length())?k:k.subseq(0,2);
    }
    
    private boolean less(OSeq k1, OSeq k2) {
        return chk(k1).toString().compareTo(chk(k2).toString())>0;
    }
    private boolean equal(OSeq k1, OSeq k2) {
        return chk(k1).toString().equals(chk(k2).toString());
    }

    private OSeq minimal(OSeq k1) {
        OSeq ret = k1;
        if (aReverse) {
            if (less(ret,k1.reverse(true))) {
                ret = k1.reverse(true);
            }
            if (aComplement) {
                if (less(ret,k1.complement())) {
                    ret = k1.complement();
                }
                if (less(ret,k1.complement().reverse(true))) {
                    ret = k1.complement().reverse(true);
                }
            }
        } else if (aComplement) {
            if (less(ret,k1.complement())) {
                ret = k1.complement();
            }
        }
        return ret;
    }
    
	private void setPointer(OSeq s, short rule) {
	    assert(s.length()==2);
		if (s.seq[1].isUniqueTerminator()) return;
		if (debug) System.out.println("setPointer:setting "+s.toString()+":"+rule);
        if (debug) System.out.println("increasing("+s.toString()+")="+minimal(s));
		pointers.put(minimal(s),rule);
	}

   private short getPointer(OSeq s) {
        assert(s.length()==2);
        if (s.seq[1].isUniqueTerminator()) return -1;
        if (debug) System.out.println("getPointer:getting "+s.toString());
        Short ret = pointers.get(minimal(s));
        if (null==ret) return -2;
        return ret;
    }

	void updatePointerToThisOcurrence(short rule, int beginPos) {
		if (0>beginPos) {
			return;
		}
		OSeq r = (OSeq) g.rule(rule);
		if (beginPos>=(r.length()-1)) {
			return;
		}
		if (r.seq[beginPos+1].isUniqueTerminator()) {
			return; 
		}
        OSeq pair = (OSeq) r.subseq(beginPos, beginPos+2);
        short oldRule = getPointer(pair);
        if (oldRule>=0) {
            /*
                r2-><1>^{R}s<1>$3
                o problema que a solu��o com reverso implica em que haja 2 referencias para o mesmo string
            */
            boolean excuse = false;
            int oldBeginPos  = ((OSeq)g.rule(oldRule)).findPairFirstOcurrence(pair,aReverse,aComplement);
            if ((aReverse ||pair.seq[0].equals(pair.seq[1])) && rule == oldRule) {
                if (oldBeginPos== (beginPos-1)) excuse = true;
            }
            if (!excuse) {
                System.err.println("oldBeginPos="+oldBeginPos);
                System.err.println("beginPos="+beginPos);
                System.err.println("rule="+rule);
                System.err.println("oldRule="+oldRule);
                System.err.println("currentFinalPos="+currentFinalPos);
                System.err.println("("+oldRule+","+oldBeginPos+") oldpair="+(OSeq) g.rule(oldRule).subseq(oldBeginPos, oldBeginPos+2));
                System.err.println("("+rule+","+beginPos+") pair="+pair);
                RuntimeException e = new RuntimeException("getPointer("+pair+")="+getPointer(pair));
                e.printStackTrace();
            }
		}
        setPointer(pair, rule);
		if (debug) System.out.println("updatePointerToThisOcurrence(rule="+rule+", beginPos="+beginPos+", pair="+pair+", )");
	}

	
	public void removePointer(short rule, int pos) {
		if (0>pos) return;
		if (pos>= g.rule(rule).length()-1) return;
        OSeq pair = (OSeq) g.rule(rule).subseq(pos, pos+2);
		if (pair.seq[1].isUniqueTerminator()) return;
		if (debug) System.out.println("pointers.remove("+pair+");");
        pointers.remove(minimal(pair));
	}
	
	void substituteTwoNodesRemovingOcurrences(
	        short currentRule, int currentBeginPos, short newrule, boolean removeCurrent, boolean removeNext) {
		if (debug) {
        //if (true) {
			System.out.println("<susbstituteTwoNodesRemovingOcurrences> currentRule="+currentRule+" currentRule="+currentRule+" newrule="+newrule);
			System.out.println("g="+g.toString());
		    MAGR.checkGrammar(g,testinput);
			DUMP_POINTERS();
		}
		removePointer(currentRule, currentBeginPos-1);
		if (removeCurrent) removePointer(currentRule, currentBeginPos);
		if (removeNext) removePointer(currentRule, currentBeginPos+1);// case of oldPosition
		assert(g.rule(currentRule).length()>(currentBeginPos+1));
		OSeq r = (OSeq) g.rule(currentRule);
		OSeq pair = r.subseq(currentBeginPos, currentBeginPos+2);
		r.seq[currentBeginPos]=ASymbol.nonTerminal(newrule);
        boolean rev=false;
        boolean comp=false;
//r.seq[currentBeginPos].setReversed(decreases(pair)^decreases((OSeq) g.rule(newrule)));
		do {
		    OSeq k1 = chk(pair);
            OSeq k2 = chk((OSeq) g.rule(newrule));
            if (equal(k1,k2)) break;
		    if (aReverse) {
	            if (equal(k1,k2.reverse(true))) {
	                rev = true;
	                break;
	            }
	            if (aComplement) {
	                if (equal(k1,k2.complement())) {
	                    comp = true;
	                    break;
	                }
	                if (equal(k1,k2.complement().reverse(true))) {
	                    rev = true;
                        comp = true;
                        break;
	                }
	            }
	        } else if (aComplement) {
	            if (equal(k1,k2.complement())) {
                    comp = true;
                    break;
	            }
	        }
		    assert(false);
		} while (false);
        r.seq[currentBeginPos].setReversed(rev);
        r.seq[currentBeginPos].setComplemented(comp);
		r.remove(currentBeginPos+1);
		if (removeCurrent)updatePointerToThisOcurrence(currentRule, currentBeginPos-1);
        if (removeCurrent) updatePointerToThisOcurrence(currentRule, currentBeginPos);//??
		if (debug) {
			System.out.println("g="+g.toString());
	        MAGR.checkGrammar(g,testinput);
            DUMP_POINTERS();
			System.out.println("</susbstituteTwoNodesRemovingOcurrences>");		
		}
	}

    public void expandUpdatingPointers(short expandingRule, int place, short expandedEliminatedRule) {
        if (debug) {
            System.out.println("<expandUpdatingPointers>");
            System.out.println("g="+g.toString());
            DUMP_POINTERS();
        }
        removePointer(expandingRule,place-1);
        removePointer(expandingRule,place);
        int size = ((OSeq)g.rule(expandedEliminatedRule)).length();
        for (int i=0; i<size; ++i) {
            removePointer(expandedEliminatedRule,i);
        }
        assert(((OSeq)g.rule(expandingRule)).seq[place].isNonterminal());
        assert(((OSeq)g.rule(expandingRule)).seq[place].getValue()==expandedEliminatedRule);
        ((OSeq)g.rule(expandingRule)).expandRuleLocal(place, (OSeq)g.rule(expandedEliminatedRule), true, true);
        updatePointerToThisOcurrence(expandingRule,place-1);
        for (int i=0; i<size; ++i) {
            updatePointerToThisOcurrence(expandingRule,place+i);
        }
        g.setRule(expandedEliminatedRule, new OSeq(""));

        if (debug) {
            System.out.println("g="+g.toString());
            DUMP_POINTERS();
            System.out.println("</expandUpdatingPointers>");
        }

    }
	

	void substituteRuleUsedOnce() {
	    if (debugRenumber) {
	        System.out.println("<substituteRuleUsedOnce>");
            System.out.println("g="+g);
            MAGR.checkGrammar(g,testinput);
            
	    }
	    updateRuleReferences();
    	for (short i=0; i<g.ruleQtt; ++i) {
    		if (references[i]==1) {
                ASymbol a = ((OSeq)g.rule(lastReferenceRule[i])).seq[lastReferencePos[i]]; 
                assert(a.isNonterminal()&&a.getValue()==i);
    			if (debug) System.out.println("***********expand "+i+" into "+lastReferenceRule[i]);
    			expandUpdatingPointers(lastReferenceRule[i], lastReferencePos[i], i);
    		}
    	}
        if (debugRenumber) {
            System.out.println("g="+g);
            MAGR.checkGrammar(g,testinput);
            System.out.println("</substituteRuleUsedOnce>");
        } else {
            if (debug) MAGR.checkGrammar(g,testinput);
        }
	}
 
	short renumberValue(ASymbol a) {
		short ret = a.getValue();
		if (a.isNonterminal()) {
			assert(1<references[ret]);
			ret = (short)(newNumber[ret]);
		}
		return ret;
	}
	
	  private OSeq renumberRhs(OSeq s) {
	    	OSeq s2 = s.clone();
	    	for (int i=0; i<s.length(); ++i) {
	    	    ASymbol n = s.seq[i];
	    		if (n.isUniqueTerminator())	break;
	    		n.setValue(renumberValue(n));
	    	}
	    	return s2;
	    }
	  
	  int newNumber[];
	  int references[];
      short lastReferenceRule[];
      int lastReferencePos[];
      private void updateRuleReferences() {
          newNumber = new int[g.ruleQtt];
          references = new int[g.ruleQtt];
          lastReferenceRule = new short[g.ruleQtt];
          lastReferencePos = new int[g.ruleQtt];
          for (short i=0; i<g.ruleQtt; ++i) {
              OSeq rhs = (OSeq) g.rule(i);
              int size = rhs.length();
              assert (rhs.seq[size-1].isUniqueTerminator());
              for (int j=0; j<size-1; ++j) {
                  if (rhs.seq[j].isNonterminal()) {
                      references[rhs.seq[j].getValue()]++;
                      lastReferenceRule[rhs.seq[j].getValue()]=i;
                      lastReferencePos[rhs.seq[j].getValue()]=j;
                  }
              }
          }
      }      
      
	  private OGrammar renumber(int currentFinalPos) {
	    if (debugRenumber) {
            System.out.println("<renumber>");
            System.out.println("g="+g);
	    }
	    updateRuleReferences();
    	boolean needToRenumber = false;
        short k = 0;
    	for (int i=0; i<g.ruleQtt; ++i) {
    	    // there is no rule used exactly once
    		assert(references[i]!=1);
    		if ((0==i)||references[i]>1) {
    			newNumber[i]=k++;
    		} else {
    			newNumber[i]=-1;
    			needToRenumber = true;
    		}
    	}
    	OGrammar ret = g;
    	if (needToRenumber) {
	    	OGrammar g2 = new OGrammar(aReverse);
	    	for (int i=0; i<g.ruleQtt; ++i) {
                if (debugRenumber) {
                    System.out.println("i="+i);
                    System.out.println("references["+i+"]="+references[i]);
                }
				if ((0==i)||references[i]>1) {
                    assert(g.rule(i).length()>1);
					OSeq rn = renumberRhs((OSeq)g.rule(i));
					g2.addRule(rn);
				}
	    	}
	    	ret = g2;
    	}
        if (debugRenumber) {
            System.out.println("g="+ret);
            System.out.println("</renumber>");
        }
        return ret;
    }

	public OGrammar process(byte buffer[], boolean verbose) {
	    if (null==testinput) {
            testinput = buffer;// para poder chamar checkGrammar de fun��es internas, quando chamado do MGR
        }
        int a =0;
        g = new OGrammar(aReverse);
        g.addRule(new OSeq(buffer));
		assert(1==g.ruleQtt);
		//Vector<Set<Pair<Short,Integer>>> 
	    short currentRule = 0;
	    currentFinalPos  = 1;
        while (currentFinalPos<g.rule(0).length()) {
            OSeq rule = (OSeq) g.rule(currentRule);
            OSeq pair = (OSeq) rule.subseq(currentFinalPos-1, currentFinalPos+1);
        	if (debug) {
                System.out.println("g.rule(0)="+g.rule(0).toString());
                System.out.println("currentFinalPos="+currentFinalPos);
        	    System.out.println("****pair="+pair.toString());
        	    DUMP_POINTERS();
        	}
        	a++;
        	int   oldBeginPos = -1;
			short oldRule = getPointer(pair);
            if (debug) System.out.println("oldRule="+oldRule);
			if (0<=oldRule) {
			    oldBeginPos  = ((OSeq)g.rule(oldRule)).findPairFirstOcurrence(pair,aReverse,aComplement);
	            if (debug) System.out.println("oldBeginPos="+oldBeginPos);
	            OSeq r = (OSeq)g.rule(oldRule); 
	            assert(
	                ( r.seq[oldBeginPos].getValue()==pair.seq[0].getValue()&&r.seq[oldBeginPos+1].getValue()==pair.seq[1].getValue())
	                || (aReverse &&
	                        ( r.seq[oldBeginPos].getValue()==pair.seq[1].getValue()&&r.seq[oldBeginPos+1].getValue()==pair.seq[0].getValue())
	                   )
	                || (aComplement)
	                );
			}
			if (-1 == oldBeginPos || (oldRule==currentRule && oldBeginPos>=currentFinalPos-2)) {
				// 1. no position found or overlaps
				if (-1 == oldBeginPos) {
                    if (debug) System.out.println("no position found");
					setPointer(pair,currentRule);
				} else {
                    if (debug) System.out.println("overlap found");
				}
				currentFinalPos++;
			} else {
				// 1. rule r with rhs p
                if (debug) {
                    System.out.println("former position found");
                    System.out.println("oldBeginPos="+oldBeginPos);
                    System.out.println("currentFinalPos="+currentFinalPos);
                    System.out.println("currentFinalPos-2="+(currentFinalPos-2));
                }
				newOcurrence(oldRule, oldBeginPos, (short)0, currentFinalPos-1);
                if (debug) System.out.println("after currentFinalPos="+currentFinalPos);
			}
			endLoop(a); 
			if (debug) MAGR.checkGrammar(g,testinput);
        }
        if (verbose) System.out.println("total iterations="+a);
		g = renumber(currentFinalPos);
		if (debug) System.out.println("g="+g);
        return g;
	}

	private void newOcurrence(short oldRule, int oldBeginPos, 
	        short currentRule, int currentBeginPos) {
		if (debug) System.out.println("<newOcurrence> oldRule="+oldRule+" oldPos="+oldBeginPos+
		            " currentRule="+currentRule+" currentBeginPos="+currentBeginPos);
		short subsituteRule;
		boolean needNewRule = false;
		int oldOcurrenceRuleSize;
		oldOcurrenceRuleSize = g.rules.get(oldRule).length();
		assert ((2+1)<=oldOcurrenceRuleSize);
		if (oldOcurrenceRuleSize>(2+1)) needNewRule = true;
		if (debug) System.out.println("@@@needNewRule="+needNewRule);	
		if (needNewRule) {
			short newRule = createNewRule(currentRule, currentBeginPos);
	        if (debug) System.out.println("currentBeginPos="+currentBeginPos);   
			substituteTwoNodesRemovingOcurrences(oldRule, oldBeginPos, newRule,true,true);
			// since previous function removes pointers, 
			// the adjustments for new rule can only happen now
	        updatePointerToThisOcurrence(newRule, 0);
			if (oldRule == currentRule) {
		        currentBeginPos--;
		        currentFinalPos--;
			}
            if (debug) System.out.println("currentBeginPos="+currentBeginPos);   
			// como a regra � nova, ela n�o pode ocorrer antes...
			subsituteRule = newRule;
		} else {
			// Does not Need to create rule and replace position by rule
			assert((2+1)==g.rules.get(oldRule).length());
			//ruleCount.set(positionRule,ruleCount.get(positionRule)+1);
			subsituteRule = oldRule;
		}
		assert(0!=subsituteRule);
		substituteTwoNodesRemovingOcurrences(currentRule,currentBeginPos,subsituteRule,false/*because new position is NOT in the table*/,false/*because next position has NOT been read yet*/);
        currentBeginPos--;
        currentFinalPos--;
		/* 
		 * newPosition.second.previous, newPosition.second MAY already be in pointer
		 * hence, this would be a new repetition
		 * to avoid recursiveness it is better to go back one position before continue
		 * therefore, it is NOT necessary to call updatePointer() here 
		 * //BUT THIS IS ONLY TRUE IF NEW POSITION IS IN RULE 0
		 */
		assert(0==currentRule);
		substituteRuleUsedOnce();
        if (debug) System.out.println("</newOcurrence>");
	}

	private short createNewRule(short currentRule, int currentBeginPos) {
		OSeq rhs = new OSeq();
		rhs = ((OSeq)g.rule(currentRule)).subseq(currentBeginPos, currentBeginPos+2);
		return g.addRule(minimal(rhs));
	}

	public byte testinput[];
	
	public static void testAssert(String input,int ruleQtd, int grammarSize,boolean rev) {
	    MAGR.aReverse = rev;
        Alg_Sequitur sq = new Alg_Sequitur();
        sq.testinput = input.getBytes();
		Grammar g = sq.process(sq.testinput,false);
		if (ruleQtd != g.ruleQtt) {
			System.err.println("g.ruleQtt="+g.ruleQtt+" expected:"+ruleQtd);
		}
		if (grammarSize!= g.size()) {
			System.err.println("g.size()="+g.size()+" expected:"+(grammarSize));
			System.err.println("g="+g.toString());
		} else {
            //System.out.println("g.size()="+g.size()+" expected:"+(grammarSize));
            //System.out.println("g="+g.toString());
		}
		MAGR.checkGrammar = true;
		MAGR.checkGrammar(g,sq.testinput);
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
        //sq.testinput = "testeateste".getBytes();
        MAGR.checkGrammar = true;
        MAGR.veryVerbose = true;
        System.out.println("Begin of tests.");
        
      
        testAssert("teste",2,5,false);//size = sum rhs (not including final terminator)
        testAssert("testea",2,6,false);
        testAssert("testeat",2,7,false);
        testAssert("testeate",2,7,false);
        testAssert("testeates",3,8,false);
        testAssert("testeatest",3,9,false);
        testAssert("testeateste",3,8,false);
        testAssert("testeatestebteste",3,10,false);
        testAssert("testeatestebtesteateste",4,11,false);
        ///////////////////////////////
        testAssert("teste",2,5,true);        
        testAssert("teset",2,5,true);
        testAssert("testea",2,6,true);
        testAssert("teseta",2,6,true);
        testAssert("testeat",2,7,true);
        testAssert("tesetat",2,7,true);
        testAssert("testeate",2,7,true);
        testAssert("tesetate",2,7,true);
        testAssert("testeates",3,8,true);
        testAssert("tesetates",3,8,true);
        testAssert("testeatest",3,9,true);
        testAssert("tesetatest",3,9,true);
        testAssert("testeateste",3,8,true);
        testAssert("tesetateste",3,9,true);
        testAssert("tesetateset",3,8,true);
        testAssert("testeatestebteste",3,10,true);
        testAssert("testeatesetbteste",4,12,true);
        testAssert("tesetatesetbteset",3,10,true);
        
        testAssert("testeatestebtesteateste",4,11,true);
        testAssert("testeatestebetsetaetset",4,11,true);
        testAssert("testeatesteetsetaetset",4,10,true);
  
        testAssert("TATAGGATATACACTGATTGTTGGCAACTATCATTA67778888AATCATAAAAAAAATATGAC",11,47,true);
        System.out.println("End of tests.");
	}

}
