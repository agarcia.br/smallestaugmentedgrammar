package tecmf;

import java.io.*;
import java.nio.file.*;
import java.util.Random;

import tecmf.gene.BinaryComplement;
import tecmf.gene.Complement;
import tecmf.gene.DNAComplement;
import tecmf.gene.RNAComplement;
import tecmf.ircoo.IRCOO_Minimizer;
import tecmf.smst.*;
import tecmf.utils.Persistent;

public class RANDOM {
    /* Iterative Repeat Replacement */

    static private String SINGLE_ALPHABET = "Exactly one of A, B, R, D, L type can be set";
    static public boolean verbose = false;
    static public int alphasize = -1;
    static public long size = -1;
    static public String type = null;
    static public String parameters = "-";
    public static String outputFile = null;


    public static void usage() {
        System.out.println("\nUsage:");
        System.out.println(
                "java -jar RANDOM.jar [-ABRDv#] [-L <alphabetSize>] <outputFile> -S size (in bytes)");
        System.out.println("where:");
        System.out.println("\t a -- print command line Args");
        System.out.println("\t A -- gen ASCII file");
        System.out.println("\t B -- gen Binary file");
        System.out.println("\t R -- gen RNA file");
        System.out.println("\t D -- gen DNA file");
        System.out.println("\t L -- alphabet size from 2 to 80");
        System.out.println("\t v -- verbose");
    }

    public static boolean processArgs(String[] args) {
        /* initialize again so that processArgs() can be repeated in tests */
        verbose = false;
        size = -1;
        type = null;
        outputFile = null;

        for (int i = 0; i < args.length; ++i) {
            String s = args[i];
            if (s.startsWith("-")) {
                parameters += s.substring(1);
                if (s.contains("A")) {
                    if ( type == null) {
                        type="A";
                    } else {
                        System.out.println(SINGLE_ALPHABET);
                    }
                }
                if (s.contains("B")) {
                    if ( type == null) {
                        type="B";
                    } else {
                        System.out.println(SINGLE_ALPHABET);
                    }
                }
                if (s.contains("R")) {
                    if ( type == null) {
                        type="R";
                    } else {
                        System.out.println(SINGLE_ALPHABET);
                    }
                }
                if (s.contains("D")) {
                    if ( type == null) {
                        type="D";
                    } else {
                        System.out.println("One of A, B, R, D, L type can be set");
                    }
                }
                if (s.contains("L")) {
                    if ( type == null) {
                        type="L";
                        try {
                            alphasize = Integer.parseInt(args[++i]);
                            if (alphasize<2 || alphasize>256) {
                                System.err.println("Invalid alphabet size:"+args[i]);
                                System.err.println("alphabet size must be from 2 to 256");
                                usage();
                                return false;
                            }
                        } catch (Exception e) {
                            System.err.println("Alphabet size format error:" + args[i]);
                            usage();
                            return false;
                        }
                    } else {
                        System.out.println("One of A, B, R, D, L type can be set");
                    }
                }
                if (s.contains("h")) {
                    usage();
                    return false;
                }
                if (s.contains("v")) {
                    verbose = true;
                }
                if (s.contains("S")) {
                    try {
                        size = Long.parseLong(args[++i]);
                    } catch (Exception e) {
                        System.err.println("Size should be in bytes format:" + args[i]);
                        usage();
                        return false;
                    }
                }
            } else if (null == outputFile) {
                outputFile = s;
            } else if ("".equals(s)) {
                //ok
            } else {
                usage();
                return false;
            }
        }
        if (null==type ) {
            System.out.println(SINGLE_ALPHABET);
            usage();
            return false;
        }
        return true;
    }

    
    public static byte values[]=null;
    static Random random = null;
    public static void initValues(String type)  {
        random = new Random();
        if ("D".equals(type)) {
            values = new byte[] {(byte)'A',(byte)'C',(byte)'G',(byte)'T'};
        } else if ("R".equals(type)) {
            values = new byte[] {(byte)'A',(byte)'C',(byte)'G',(byte)'U'};
        } else if ("B".equals(type)) {
            values = new byte[256];
            for (int i=0; i<256;++i) {
               values[i]= (byte)(i-128);
            }
        } else if ("L".equals(type)&&(alphasize<110)){
            values = new byte[alphasize];
            for (int i=0; i<alphasize;++i) {
               values[i]= (byte)(48+i); // Starts from '0', because it is even
            }
        } else if ("L".equals(type)){
            values = new byte[alphasize];
            for (int i=0; i<alphasize;++i) {
               values[i]= (byte)(i); 
            }
        } else  {
            assert("A".equals(type));
            throw new RuntimeException("ASCII generation not implemented");
            // Use letter AND space frequency in english language
        }    
    }

    public static int generateByte()  {
        return values[random.nextInt(values.length)];
    }

    public static void generate() throws FileNotFoundException, IOException {
        initValues(type);
System.out.println("size="+size);        
System.out.println("outputFile="+outputFile);        
        try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outputFile)))) {
            for (int i = 0; i < size; ++i) {
                dos.writeByte(generateByte());
            }
        } catch (IOException e) {
            System.err.println("outputFile="+outputFile);        
            e.printStackTrace(System.err);
        }
    }

   
    public static void main(String[] args) throws FileNotFoundException, IOException {
        if (!processArgs(args))
            return;
        generate();
    }

}
