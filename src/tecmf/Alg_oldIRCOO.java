package tecmf;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import tecmf.smst.OGST;
import tecmf.ircoo.IRCOO_Minimizer;
import tecmf.smst.Grammar;
import tecmf.smst.OGrammar;
import tecmf.smst.ONodeInfo;
import tecmf.smst.OOSC;
import tecmf.smst.OSeq;
import tecmf.smst.OSeqLengthComparator;
import tecmf.utils.P_OSeqSet;
import tecmf.utils.Persistent;

public class Alg_oldIRCOO extends Alg {
    /* Iterative Repeat Replacement */


    public String oscType = null;
    public String alg = null;
    public boolean aReverse;
    public boolean aShift;
    public boolean pruneUnused;

    P_OSeqSet blacklist = null;

    public Alg_oldIRCOO(String oscType, boolean aReverse, boolean aShift, String alg, boolean pruneUnused) {
        super();
        this.oscType = oscType;
        this.aReverse = aReverse;
        this.aShift = aShift;
        this.alg = alg;
        this.pruneUnused = pruneUnused;
        this.debug=false;
    }

    private void DUMP_BLACKLIST() {
        int i = 0;
        System.out.println(">>>>>");
        for (OSeq s : blacklist.obj) {
            System.out.println("" + (++i) + ":" + s.toString());
        }
        System.out.println("<<<<<");
    }

    public Grammar process(byte buffer[], boolean verbose) {
        OGrammar g;
        if (null != MAGR.loadPrefix) {
            g = (OGrammar) Persistent.load(OGrammar.class, MAGR.loadPrefix);
            blacklist = (P_OSeqSet) Persistent.load(P_OSeqSet.class, MAGR.loadPrefix);
        } else {
            g = new OGrammar(true);
            g.addRule(new OSeq(buffer));
            this.blacklist = new P_OSeqSet();
            blacklist.obj = new TreeSet<OSeq>(new OSeqLengthComparator());
        }
        int a = 0;
        IRCOO_Minimizer ir = null;
        boolean gWasUpdated = true;
        OGST gst = null;
        OOSC oosc = null;
        while (true) {
            a++;
            if (debug)
                System.out.println("GRAMMAR:\n g=" + g.toString());
            if (debug)
                System.out.println("g.size()=" + g.size());
            // 1. montar Suffix Tree
            if (gWasUpdated) {
                gst = g.getYieldGST(aReverse);
                oosc = OOSC.getOSC(oscType, gst, blacklist.obj, g);
            }
            // 2. encontrar maior subpadrao
            ONodeInfo ni = oosc.selectOcurrence();
            // 3 se nao encontrou, sair
            if (!ni.nonOverlaopingOcurrences)
                break;
            // 4. incluir nova regra na gramatica
            OSeq p = ni.node.getPattern();
            if (debug)
                System.out.println("pattern =" + p.toString());
            if (debug && (-2 == a || -1 == a)) {
                debug = true;
                IRCOO_Minimizer.debug = true;
            }
            OGrammar g2;
            if (pruneUnused) {
                // In this case minimize grammar based on previous grammar plus new pattern
                g2 = g.clone();
                g2.addRule(p);
            } else { // !pruneUnused
                // In this case minimize grammar based on ALL previous pattern
                g2 = new OGrammar(true);
                g2.addRule(new OSeq(buffer));
                for (OSeq os : blacklist.obj) {
                    g2.addRule(os);
                }
            }
            ir = new IRCOO_Minimizer(g2, aReverse);
            // 5. minimiza todos os yields[r];
            ir.ircooMinimizeRhs();
            // 6. Elimina as regras usadas 0 ou 1 vez.
            if (IRCOO_Minimizer.debug) {
                System.out.println("g=" + g);
                System.out.println("g2=" + g2);
            }
            OGrammar g3 = ir.renumber();
            if (IRCOO_Minimizer.debug) {
                System.out.println("g3=" + g3);
            }
            // if (debug) System.out.println("GRAMMAR:\n g3="+g3.toString());
            gWasUpdated = false;
            if (debug)
                System.out.println("g.size()=" + g.size());
            if (debug)
                System.out.println("g3.size()=" + g3.size());
            if (g3.size() < g.size()) {
                gWasUpdated = true;
                g = g3;
                if (debug)
                    System.out.println("Grammar updated because g3.size()<g.size()");
            } else {
                if (debug)
                    System.out.println("Grammar not updated because g3.size()>=g.size()");
            }
            if (debug || (verbose && (0 == a % 50))) {
                String pa = p.toString();
                String etc = "";
                int len = pa.length();
                if (len > 10) {
                    len = 10;
                    etc = "...";
                }
                System.out.println("iterations=" + a + "; numrules=" + g.ruleQtt + " pattern =" + pa.substring(0, len)
                        + etc + " pattern.length()=" + p.toString().length());
                System.out.println("blacklist.obj.size()=" + blacklist.obj.size());
            }
            if (debug)
                DUMP_BLACKLIST();
            endLoop(a);
            Persistent.conditionalSave(MAGR.saveInterval, g, blacklist);
        }
        if (verbose)
            System.out.println("total iterations=" + a);
        if (MAGR.savebinary)
            g.saveBinary();
        return g;
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
    }

}
